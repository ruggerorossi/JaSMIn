/**
 * The MonitorUI class definition.
 *
 * The MonitorUI abstracts the handling of the player related ui elements.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.MonitorUI');

goog.require('JaSMIn');
goog.require('JaSMIn.LogPlayer');
goog.require('JaSMIn.MonitorModel');
// goog.require('JaSMIn.MonitorSettings');
goog.require('JaSMIn.UI');
goog.require('JaSMIn.UI.DnDHandler');
goog.require('JaSMIn.UI.FullscreenManager');
goog.require('JaSMIn.UI.InputController');
goog.require('JaSMIn.UI.LoadingBar');
goog.require('JaSMIn.UI.PlayerUI');
goog.require('JaSMIn.UI.ResourceExplorer');
goog.require('JaSMIn.UI.WelcomeOverlay');
goog.require('JaSMIn.WorldLoader');



/**
 * MonitorUI Constructor
 *
 * @constructor
 * @param {!JaSMIn.MonitorModel} model the monitor model
 * @param {!Element} container the monitor root dom element
 */
JaSMIn.UI.MonitorUI = function(model, container) {

  /**
   * The monitor model.
   * @type {!JaSMIn.MonitorModel}
   */
  this.model = model;

  /**
   * The player root element.
   * @type {!Element}
   */
  this.domElement = JaSMIn.UI.createDiv('jsm-root');
  container.appendChild(this.domElement);

  /**
   * The drag and drop handler.
   * @type {!JaSMIn.UI.DnDHandler}
   */
  this.dndHandler = new JaSMIn.UI.DnDHandler();
  this.dndHandler.onNewFilesDropped = function () {
    var mm = model;

    return function (files) {
      mm.loadFiles(files);
    };
  }();

  /**
   * The fullscreen manager.
   * @type {!JaSMIn.UI.FullscreenManager}
   */
  this.fullscreenManager = new JaSMIn.UI.FullscreenManager(this.domElement);

  /**
   * The explorer root element.
   * @type {!Element}
   */
  this.explorerRoot = JaSMIn.UI.createDiv('explorer-root');
  this.domElement.appendChild(this.explorerRoot);

  /**
   * The root divider element.
   * @type {!Element}
   */
  this.rootDivider = JaSMIn.UI.createDiv('root-divider');
  this.domElement.appendChild(this.rootDivider);

  /**
   * The monitor root element.
   * @type {!Element}
   */
  this.monitorRoot = JaSMIn.UI.createDiv('monitor-root');
  this.domElement.appendChild(this.monitorRoot);

  /**
   * The resource explorer.
   * @type {!JaSMIn.UI.ResourceExplorer}
   */
  this.resourceExplorer = new JaSMIn.UI.ResourceExplorer(this.model);
  this.explorerRoot.appendChild(this.resourceExplorer.domElement);

  /**
   * The webgl panel instance, handling the webgl rendering.
   * @type {!JaSMIn.UI.GLPanel}
   */
  this.glPanel = new JaSMIn.UI.GLPanel(this.monitorRoot);
  this.glPanel.onNewRenderCycle = this.handleNewRenderCycle.bind(this);
  this.glPanel.scene = this.model.world.scene;
  this.glPanel.glInfoBoard.setVisible(this.model.settings.monitorConfig.glInfoEnabled);
  this.glPanel.renderer.shadowMap.enabled = this.model.settings.monitorConfig.shadowsEnabled;
  this.glPanel.renderInterval = 30;

  /**
   * The mouse and keyboard input controller.
   * @type {!JaSMIn.UI.InputController}
   */
  this.inputController = new JaSMIn.UI.InputController(this.model, this.glPanel, this.fullscreenManager, this.dndHandler);
  this.monitorRoot.appendChild(this.inputController.domElement);

  /**
   * The top loading bar.
   * @type {!JaSMIn.UI.LoadingBar}
   */
  this.loadingBar = new JaSMIn.UI.LoadingBar(this.model.logPlayer.gameLogLoader);
  this.monitorRoot.appendChild(this.loadingBar.domElement);

  /**
   * The welcome overlay (providing local file selection).
   * @type {!JaSMIn.UI.WelcomeOverlay}
   */
  this.welcomeOverlay = new JaSMIn.UI.WelcomeOverlay(this.dndHandler);
  this.monitorRoot.appendChild(this.welcomeOverlay.domElement);

  /**
   * The player bar.
   * @type {!JaSMIn.UI.PlayerUI}
   */
  this.playerUI = new JaSMIn.UI.PlayerUI(this.model, this.fullscreenManager);
  this.monitorRoot.appendChild(this.playerUI.domElement);



  /** @type {!Function} */
  this.handleRevealExplorerListener = this.showExplorer.bind(this);
  /** @type {!Function} */
  this.handleHideExplorerListener = this.hideExplorer.bind(this);
  /** @type {!Function} */
  this.handleAutoSizeExplorerListener = this.autoSizeExplorer.bind(this);

  /**
   * The reveal explorer button.
   * @type {!Element}
   */
  this.revealExplorerBtn = JaSMIn.UI.createPlayerButton('&nbsp;&nbsp;&gt;', 'reveal-explorer-btn', 'Show Resource Explorer', this.handleRevealExplorerListener, true);
  this.domElement.appendChild(this.revealExplorerBtn);
  JaSMIn.UI.setVisibility(this.revealExplorerBtn, false);

  /**
   * The hide explorer button.
   * @type {!Element}
   */
  this.hideExplorerBtn = JaSMIn.UI.createPlayerButton('&lt;&nbsp;&nbsp;', 'hide-explorer-btn', 'Hide Resource Explorer', this.handleHideExplorerListener, true);
  this.rootDivider.appendChild(this.hideExplorerBtn);


  /** @type {!Function} */
  this.handleMonitorStateChangeListener = this.handleMonitorStateChange.bind(this);

  /** @type {!Function} */
  this.handleEWResizeStartListener = this.handleEWResizeStart.bind(this);
  /** @type {!Function} */
  this.handleEWResizeEndListener = this.handleEWResizeEnd.bind(this);
  /** @type {!Function} */
  this.handleEWResizeListener = this.handleEWResize.bind(this);

  /** @type {!Function} */
  this.handleMonitorConfigChangeListener = this.handleMonitorConfigChange.bind(this);

  /** @type {!Function} */
  this.handleWorldChangeListener = this.handleWorldChange.bind(this);


  // Add monitor model event listener
  this.model.addEventListener(JaSMIn.MonitorModelEvents.STATE_CHANGE, this.handleMonitorStateChangeListener);

  // Add root divider event listeners
  this.rootDivider.addEventListener('mousedown', this.handleEWResizeStartListener, false);
  this.rootDivider.addEventListener('dblclick', this.handleAutoSizeExplorerListener, false);

  // Add monitor config change lister
  this.model.settings.monitorConfig.addEventListener(JaSMIn.MonitorConfigurationEvents.CHANGE, this.handleMonitorConfigChangeListener);

  // Add world change lister
  this.model.world.addEventListener(JaSMIn.WorldEvents.CHANGE, this.handleWorldChangeListener);



  /** @type {!Function} */
  this.handleResizeListener = this.handleResize.bind(this);

  // Add window resize & beforeunload listener
  window.addEventListener('resize', this.handleResizeListener);
  window.addEventListener('beforeunload', function() {
    var mm = model;

    return function (evt) {
      mm.settings.save();
    };
  }());


  // Check for embedded mode
  if (this.model.embedded) {
    this.hideExplorer();

    // Hide welcome overlay
    this.welcomeOverlay.setVisible(false);
  }
};






/**
 * World->"change" event listener.
 * This event listener is triggered when the world representation has changed.
 *
 * @param {!Object} evt the change event
 * @return {void}
 */
JaSMIn.UI.MonitorUI.prototype.handleWorldChange = function(evt) {
  this.inputController.camCon.setAreaOfInterest(this.model.world.field.fieldDimensions);
  this.inputController.camCon.setPredefinedPose();
  this.inputController.domElement.focus();
};



/**
 *
 * @param {!number} deltaT the time passed since the last render cycle in milliseconds
 * @return {void}
 */
JaSMIn.UI.MonitorUI.prototype.handleNewRenderCycle = function(deltaT) {
  // Do stuff...

  // Forward call to player
  if (this.model.state === JaSMIn.MonitorStates.REPLAY) {
    this.model.logPlayer.update(deltaT);
  }
};



/**
 *
 *
 * @param {!Event} evt the mouse event
 * @return {void}
 */
JaSMIn.UI.MonitorUI.prototype.handleEWResizeStart = function(evt) {
  // Prevent scrolling, text-selection, etc.
  evt.preventDefault();
  evt.stopPropagation();

  this.domElement.style.cursor = 'ew-resize';
  this.domElement.addEventListener('mousemove', this.handleEWResizeListener, false);
  this.domElement.addEventListener('mouseup', this.handleEWResizeEndListener, false);
};



/**
 *
 *
 * @param {!Event} evt the mouse event
 * @return {void}
 */
JaSMIn.UI.MonitorUI.prototype.handleEWResizeEnd = function(evt) {
  this.domElement.style.cursor = '';
  this.domElement.removeEventListener('mousemove', this.handleEWResizeListener, false);
  this.domElement.removeEventListener('mouseup', this.handleEWResizeEndListener, false);

  var percent = 100 * (evt.clientX + 2) / this.domElement.offsetWidth;

  if (percent < 5) {
    this.hideExplorer();
  }
};



/**
 *
 *
 * @param {!Event} evt the mouse event
 * @return {void}
 */
JaSMIn.UI.MonitorUI.prototype.handleEWResize = function(evt) {
  // Prevent scrolling, text-selection, etc.
  evt.preventDefault();
  evt.stopPropagation();

  var percent = 100 * (evt.clientX + 2) / this.domElement.offsetWidth;

  // Limit explorer width to a maximum of 50%
  if (percent > 50) {
    percent = 50;
  }

  // Hide explorer if now width is sell than 5%
  if (percent < 5) {
    this.explorerRoot.style.width = '0px';
    this.monitorRoot.style.width = 'calc(100% - 3px)';
  } else {
    this.explorerRoot.style.width = 'calc(' + percent + '% - 3px)';
    this.monitorRoot.style.width = '' + (100 - percent) + '%';
  }

  this.glPanel.autoResize();
};



/**
 * Handle resizing of window.
 *
 * @param {!Event} evt the resize event
 * @return {void}
 */
JaSMIn.UI.MonitorUI.prototype.handleResize = function(evt) {
  this.glPanel.autoResize();
};



/**
 * Automatically resize the resource explorer to its reuired width or the maximum width of 50%.
 *
 * @return {void}
 */
JaSMIn.UI.MonitorUI.prototype.autoSizeExplorer = function() {
  if (this.explorerRoot.scrollWidth === this.explorerRoot.offsetWidth) {
    // Nothing to scroll, thus nothing to resize
    return;
  }

  // Show explorer and divider
  JaSMIn.UI.setVisibility(this.explorerRoot, true);
  JaSMIn.UI.setVisibility(this.rootDivider, true);

  var percent = 100 * (this.explorerRoot.scrollWidth + 3) / this.domElement.offsetWidth;

  if (percent > 50) {
    percent = 50;
  }

  // Resize containers
  this.explorerRoot.style.width = 'calc(' + percent + '% - 3px)';
  this.monitorRoot.style.width = '' + (100 - percent) + '%';
  this.glPanel.autoResize();

  // Hide reveal explorer button
  JaSMIn.UI.setVisibility(this.revealExplorerBtn, false);
};



/**
 * Show the resource explorer.
 *
 * @return {void}
 */
JaSMIn.UI.MonitorUI.prototype.showExplorer = function() {
  if (this.model.embedded) {
    return;
  }

  // Show explorer and divider
  JaSMIn.UI.setVisibility(this.explorerRoot, true);
  JaSMIn.UI.setVisibility(this.rootDivider, true);

  // Resize containers
  this.explorerRoot.style.width = 'calc(25% - 3px)';
  this.explorerRoot.scrollLeft = 0;
  this.monitorRoot.style.width = '75%';
  this.glPanel.autoResize();

  // Hide reveal explorer button
  JaSMIn.UI.setVisibility(this.revealExplorerBtn, false);
};



/**
 * Hide the resource explorer.
 *
 * @return {void}
 */
JaSMIn.UI.MonitorUI.prototype.hideExplorer = function() {
    // Hide explorer and divider
    JaSMIn.UI.setVisibility(this.explorerRoot, false);
    JaSMIn.UI.setVisibility(this.rootDivider, false);

    // Maximize monitor container
    this.monitorRoot.style.width = '100%';
    this.glPanel.autoResize();

    // Show reveal explorer button if not in embedded mode
    JaSMIn.UI.setVisibility(this.revealExplorerBtn, !this.model.embedded);
};



/**
 * MonitorConfiguration->"change" event handler.
 * This event handler is triggered when a property of the monitor configuration has changed.
 *
 * @param  {!Object} evt the event
 * @return {void}
 */
JaSMIn.UI.MonitorUI.prototype.handleMonitorConfigChange = function(evt) {
  var config = this.model.settings.monitorConfig;

  switch (evt.property) {
    case JaSMIn.MonitorConfigurationProperties.SHADOWS_ENABLED:
      this.model.world.setShadowsEnabled(config.shadowsEnabled);
      this.glPanel.renderer.shadowMap.enabled = config.shadowsEnabled;
      break;
    case JaSMIn.MonitorConfigurationProperties.TEAM_COLORS_ENABLED:
    case JaSMIn.MonitorConfigurationProperties.TEAM_COLOR_LEFT:
    case JaSMIn.MonitorConfigurationProperties.TEAM_COLOR_RIGHT:
      this.playerUI.updateTeamColors();
      break;
    case JaSMIn.MonitorConfigurationProperties.GL_INFO_ENABLED:
      this.glPanel.glInfoBoard.setVisible(config.glInfoEnabled);
      break;
  }
};



/**
 * MonitorModel->"state-change" event listener.
 * This event listener is triggered when the monitor model state has changed.
 *
 * @param {!Object} evt the change event
 * @return {void}
 */
JaSMIn.UI.MonitorUI.prototype.handleMonitorStateChange = function(evt) {
  if (evt.newState !== JaSMIn.MonitorStates.INIT) {
    this.welcomeOverlay.setVisible(false);
    this.glPanel.renderInterval = 1;
    this.glPanel.renderTTL = 1;
  } else {
    this.glPanel.renderInterval = 30;
  }
};
