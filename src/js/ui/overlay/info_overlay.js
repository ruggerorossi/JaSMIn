/**
 * The InfoOverlay class definition.
 *
 * The InfoOverlay abstracts
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.InfoOverlay');

goog.require('JaSMIn.UI');



/**
 * InfoOverlay Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Overlay}
 */
JaSMIn.UI.InfoOverlay = function() {
  goog.base(this, 'info-pane centered');

  this.innerElement.innerHTML = '<h1>JaSMIn</h1>' +
      '<h4>Javascript Soccer Monitor Interface</h4>' +
      '<h5>v' + JaSMIn.REVISION + '</h5>' +
      '<a href="https://gitlab.com/robocup.info/JaSMIn" target="_blank">JaSMIn on GitLab</a>' +
      '<h6>Author</h6>' +
      '<span class="author">Stefan Glaser</span>' +
      '<h6>Acknowledgements</h6>' +
      '<span class="acknowledgement">JaSMIn is using <a href="https://www.threejs.org">threejs</a> for webgl rendering.</span>' +
      '<span class="acknowledgement">The 3D models and textures are partially taken from ' +
        '<a href="https://github.com/magmaOffenburg/RoboViz">RoboViz</a>' +
        ', respectively <a href="https://sourceforge.net/projects/simspark/">SimSpark</a>.' +
      '</span>';
};
goog.inherits(JaSMIn.UI.InfoOverlay, JaSMIn.UI.Overlay);
