/**
 * The SettingsOverlay class definition.
 *
 * The SettingsOverlay abstracts
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.SettingsOverlay');

goog.require('JaSMIn.MonitorConfiguration');
goog.require('JaSMIn.UI.SingleChoiceItem');
goog.require('JaSMIn.UI.ToggleItem');



/**
 * SettingsOverlay Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Overlay}
 * @param {!JaSMIn.MonitorConfiguration} config the monitor config
 */
JaSMIn.UI.SettingsOverlay = function(config) {
  goog.base(this, 'jsm-settings');

  /**
   * The monitor config.
   * @type {!JaSMIn.MonitorConfiguration}
   */
  this.config = config;


  var scope = this;

  /**
   * The main menu list.
   * @type {!Element}
   */
  this.mainMenu = JaSMIn.UI.createUL('jsm-menu');
  this.innerElement.appendChild(this.mainMenu);

  /**
   * The interpolate state item.
   * @type {!JaSMIn.UI.ToggleItem}
   */
  this.interpolateItem = new JaSMIn.UI.ToggleItem('Interpolation', 'On', 'Off', config.interpolateStates, 'item');
  this.interpolateItem.onChanged = function(isOn) {
    config.setInterpolateStates(isOn);
  };
  this.mainMenu.appendChild(this.interpolateItem.domElement);

  /**
   * The shadows enabeld state item.
   * @type {!JaSMIn.UI.ToggleItem}
   */
  this.shadowsItem = new JaSMIn.UI.ToggleItem('Shadows', 'On', 'Off', config.shadowsEnabled, 'item');
  this.shadowsItem.onChanged = function(isOn) {
    config.setShadowsEnabled(isOn);
  };
  this.mainMenu.appendChild(this.shadowsItem.domElement);

  /**
   * The monitor statistics state item.
   * @type {!JaSMIn.UI.ToggleItem}
   */
  this.statisticsItem = new JaSMIn.UI.ToggleItem('Monitor Statistics', 'On', 'Off', config.glInfoEnabled, 'item');
  this.statisticsItem.onChanged = function(isOn) {
    config.setGLInfoEnabled(isOn);
  };
  this.mainMenu.appendChild(this.statisticsItem.domElement);

  /**
   * The team colors enabled state item.
   * @type {!JaSMIn.UI.ToggleItem}
   */
  this.teamColorsItem = new JaSMIn.UI.ToggleItem('Team Colors', 'Fix', 'Auto', config.teamColorsEnabled, 'item');
  this.teamColorsItem.onChanged = function(isOn) {
    config.setTeamColorsEnabled(isOn);
    scope.teamColorChooserItem.style.height = isOn ? scope.teamColorChooserItem.scrollHeight + 'px' : '0px';
  };
  this.mainMenu.appendChild(this.teamColorsItem.domElement);

  /**
   * The team color chooser item.
   * @type {!Element}
   */
  this.teamColorChooserItem = JaSMIn.UI.createDiv('collapsable');
  this.teamColorChooserItem.onclick = function(event) { event.stopPropagation(); };

  if (!config.teamColorsEnabled) {
    this.teamColorChooserItem.style.height = '0px';
  }

  /**
   * The left team color chooser.
   * @type {!Element}
   */
  this.leftTeamColorChooser = JaSMIn.UI.createColorChooser('#' + config.leftTeamColor.getHexString(), 'Left team color', 'team-color');
  this.leftTeamColorChooser.onchange = function() {
    config.setTeamColor(scope.leftTeamColorChooser.value, true);
  };

  this.rightTeamColorChooser = JaSMIn.UI.createColorChooser('#' + config.rightTeamColor.getHexString(), 'Right team color', 'team-color');
  this.rightTeamColorChooser.onchange = function() {
    config.setTeamColor(scope.rightTeamColorChooser.value, false);
  };
  this.teamColorChooserItem.appendChild(this.leftTeamColorChooser);
  this.teamColorChooserItem.appendChild(this.rightTeamColorChooser);
  this.teamColorsItem.domElement.appendChild(this.teamColorChooserItem);



  // -------------------- Listeners -------------------- //
  /** @type {!Function} */
  this.handleConfigChangeListener = this.handleConfigChange.bind(this);

  // Add config change listeners
  this.config.addEventListener(JaSMIn.MonitorConfigurationEvents.CHANGE, this.handleConfigChangeListener);
};
goog.inherits(JaSMIn.UI.SettingsOverlay, JaSMIn.UI.Overlay);







/**
 * Handle configuration change.
 *
 * @param {!Object} evt the change event
 * @return {void}
 */
JaSMIn.UI.SettingsOverlay.prototype.handleConfigChange = function(evt) {
  switch (evt.property) {
    case JaSMIn.MonitorConfigurationProperties.INTERPOLATE_STATES:
      this.applyInterpolationSettings();
      break;
    case JaSMIn.MonitorConfigurationProperties.TEAM_COLORS_ENABLED:
    case JaSMIn.MonitorConfigurationProperties.TEAM_COLOR_LEFT:
    case JaSMIn.MonitorConfigurationProperties.TEAM_COLOR_RIGHT:
      this.applyTeamColorSettings();
      break;
    case JaSMIn.MonitorConfigurationProperties.SHADOWS_ENABLED:
      this.applyShadowSettings();
      break;
    case JaSMIn.MonitorConfigurationProperties.GL_INFO_ENABLED:
      this.applyGLInfoSettings();
      break;
  }
};



/**
 * Apply team color settings.
 *
 * @return {void}
 */
JaSMIn.UI.SettingsOverlay.prototype.applyTeamColorSettings = function() {
  var isOn = this.config.teamColorsEnabled;

  this.teamColorsItem.setState(isOn);
  this.teamColorChooserItem.style.height = isOn ? this.teamColorChooserItem.scrollHeight + 'px' : '0px';
  this.leftTeamColorChooser.value = '#' + this.config.leftTeamColor.getHexString();
  this.rightTeamColorChooser.value = '#' + this.config.rightTeamColor.getHexString();
};



/**
 * Apply shadow setting.
 *
 * @return {void}
 */
JaSMIn.UI.SettingsOverlay.prototype.applyShadowSettings = function() {
  this.shadowsItem.setState(this.config.shadowsEnabled);
};



/**
 * Apply interpolate states setting.
 *
 * @return {void}
 */
JaSMIn.UI.SettingsOverlay.prototype.applyInterpolationSettings = function() {
  this.interpolateItem.setState(this.config.interpolateStates);
};



/**
 * Apply monitor info settings.
 *
 * @return {void}
 */
JaSMIn.UI.SettingsOverlay.prototype.applyGLInfoSettings = function() {
  this.statisticsItem.setState(this.config.glInfoEnabled);
};
