/**
 * The HelpOverlay class definition.
 *
 * The HelpOverlay abstracts
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.HelpOverlay');

goog.require('JaSMIn.UI');



/**
 * HelpOverlay Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Overlay}
 */
JaSMIn.UI.HelpOverlay = function() {
  goog.base(this, 'help-pane centered');

  this.innerElement.innerHTML = '<h1>HELP...</h1><h4>I need somebody...</h4>';
};
goog.inherits(JaSMIn.UI.HelpOverlay, JaSMIn.UI.Overlay);
