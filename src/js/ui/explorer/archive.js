/**
 * The Archive class definition.
 *
 * The Archive provides browsing capabilities to one remote archive.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.Archive');

goog.require('JaSMIn.UI');



/**
 * Archive Constructor
 *
 * @constructor
 * @param {!string} url the archive url
 * @param {!string} label the archive label
 */
JaSMIn.UI.Archive = function(url, label) {
  /**
   * The archive url.
   * @type {!string}
   */
  this.archiveURL = url;


  /**
   * The game log selection callback.
   * @type {!Function | undefined}
   */
  this.gameLogSelectionCallback = undefined;

  /**
   * The playlist selection callback.
   * @type {!Function | undefined}
   */
  this.playlistSelectionCallback = undefined;


  /** @type {!Function} */
  this.loadFolderListener = this.loadFolder.bind(this);
  /** @type {!Function} */
  this.loadGameLogListener = this.loadGameLog.bind(this);
  /** @type {!Function} */
  this.loadPlaylistListener = this.loadPlaylist.bind(this);


  /**
   * The component root element.
   * @type {!Element}
   */
  this.domElement = this.createFolderItem(label, '/', 'archive-root');
};



/**
 * Create a folder item.
 *
 * @param {!string} label the folder label
 * @param {!string} path the folder path
 * @param {!string=} className the item css class string
 * @return {!Element} the new folder item
 */
JaSMIn.UI.Archive.prototype.createFolderItem = function(label, path, className) {
  var liClassName = 'folder new';
  if (className !== undefined) {
    liClassName += ' ' + className;
  }

  var newItem = JaSMIn.UI.createLI(liClassName);
  newItem.dataset.path = path;

  // Add folder label
  var titleLbl = JaSMIn.UI.createSpan(label, 'title no-text-select');
  titleLbl.tabIndex = 0;
  titleLbl.addEventListener('click', this.loadFolderListener, false);
  titleLbl.addEventListener('keydown', this.loadFolderListener, false);
  newItem.appendChild(titleLbl);

  // Check for top-level folder
  if (path === '/') {
    // Set tool tip
    titleLbl.title = this.archiveURL;

    // Create remove archive button
    var btn = JaSMIn.UI.createButton('Del', 'remove-btn', 'Remove "' + this.archiveURL + '" from list of archives.');
    btn.addEventListener('click', JaSMIn.UI.Archive.removeArchive, false);
    btn.addEventListener('keydown', JaSMIn.UI.Archive.removeArchive, false);
    titleLbl.appendChild(btn);
  }

  return newItem;
};



/**
 * Create a game log item.
 *
 * @param {!string} label the game log label
 * @param {!string} path the game log path
 * @param {!string} className the additional game log item class
 * @return {!Element} the new game log item
 */
JaSMIn.UI.Archive.prototype.createGameLogItem = function(label, path, className) {
  var newItem = JaSMIn.UI.createLI('game-log' + ' ' + className);
  newItem.dataset.path = path;

  // Add game log label
  var titleLbl = JaSMIn.UI.createSpan(label, 'title no-text-select');
  titleLbl.tabIndex = 0;
  titleLbl.title = label;
  titleLbl.addEventListener('click', this.loadGameLogListener, false);
  titleLbl.addEventListener('keydown', this.loadGameLogListener, false);
  newItem.appendChild(titleLbl);

  return newItem;
};



/**
 * Create a playlist item.
 *
 * @param {!string} label the playlist label
 * @param {!string} path the playlist path
 * @return {!Element} the new playlist item
 */
JaSMIn.UI.Archive.prototype.createPlaylistItem = function(label, path) {
  var newItem = JaSMIn.UI.createLI('playlist');
  newItem.dataset.path = path;

  // Add game log label
  var titleLbl = JaSMIn.UI.createSpan(label, 'title no-text-select');
  titleLbl.tabIndex = 0;
  titleLbl.title = label;
  titleLbl.addEventListener('click', this.loadPlaylistListener, false);
  titleLbl.addEventListener('keydown', this.loadPlaylistListener, false);
  newItem.appendChild(titleLbl);

  return newItem;
};



/**
 * Action handler for loading a folder.
 *
 * @param {!Event} evt the click event
 * @return {void}
 */
JaSMIn.UI.Archive.prototype.loadFolder = function(evt) {
  if (!JaSMIn.UI.isButtonAction(evt)) {
    return;
  }

  var item = evt.target.parentNode;
  var path = item.dataset.path;
  var scope = this;

  var handleLoad = function() {
    var archive = scope;
    var folderItem = item;

    return function (evt) {
      var newClass = '';

      if (evt.target.status === 200 || evt.target.status === 0) {
        // Successfully loaded
        /** @type {!Object} */
        var listing = {};

        try {
          listing = /** @type {!Object} */ (JSON.parse(evt.target.response));
        } catch(e) {
          // Parsing error
          console.log(e);
        }

        if (listing['type'] === "archive") {
          var sublist = JaSMIn.UI.createUL('folder-listing');
          var folders = listing['folders'];
          var replays = listing['replays'];
          var sserverLogs = listing['sserverlogs'];
          var playlists = listing['playlists'];

          if (folders !== undefined) {
            for (var i = 0; i < folders.length; ++i) {
              sublist.appendChild(archive.createFolderItem(folders[i]['label'], folders[i]['path']));
            }
          }

          if (replays !== undefined) {
            for (i = 0; i < replays.length; ++i) {
              sublist.appendChild(archive.createGameLogItem(replays[i]['label'], replays[i]['path'], 'replay'));
            }
          }

          if (sserverLogs !== undefined) {
            for (i = 0; i < sserverLogs.length; ++i) {
              sublist.appendChild(archive.createGameLogItem(sserverLogs[i]['label'], sserverLogs[i]['path'], 'sserver-log'));
            }
          }

          if (playlists !== undefined) {
            for (i = 0; i < playlists.length; ++i) {
              sublist.appendChild(archive.createPlaylistItem(playlists[i]['label'], playlists[i]['path']));
            }
          }

          if (sublist.children.length > 0) {
            folderItem.appendChild(sublist)
            newClass = 'expanded';

            var titleLbl = JaSMIn.UI.filterElements(folderItem.childNodes, 'SPAN')[0];
            titleLbl.addEventListener('click', JaSMIn.UI.Archive.toggleExpand, false);
            titleLbl.addEventListener('keydown', JaSMIn.UI.Archive.toggleExpand, false);
          } else {
            newClass = 'empty';

            JaSMIn.UI.filterElements(folderItem.childNodes, 'SPAN')[0].tabIndex = -1;
          }
        } else {
          // Invalid response
        }
      } else if (evt.target.status === 404) {
        // Archive not found
        newClass = 'not-found';

        JaSMIn.UI.filterElements(folderItem.childNodes, 'SPAN')[0].tabIndex = -1;
      } else {
        // Error during loading
        console.log('Error ajax resonse for "' + folderItem.dataset.path + '"!');
        newClass = 'error';

        // Add load listener again
        var titleLbl = JaSMIn.UI.filterElements(folderItem.childNodes, 'SPAN')[0];
        titleLbl.addEventListener('click', archive.loadFolderListener, false);
        titleLbl.addEventListener('keydown', archive.loadFolderListener, false);
      }

      folderItem.className = folderItem.className.replace('loading', newClass);
    };
  }();

  var handleError = function() {
    var archive = scope;
    var folderItem = item;

    return function (evt) {
      console.log('Error ajax resonse for "' + folderItem.dataset.path + '"!');
      folderItem.className = folderItem.className.replace('loading', 'error');

      // Add load listener again
      var titleLbl = JaSMIn.UI.filterElements(folderItem.childNodes, 'SPAN')[0];
      titleLbl.addEventListener('click', archive.loadFolderListener, false);
      titleLbl.addEventListener('keydown', archive.loadFolderListener, false);
    };
  }();

  var xhr = new XMLHttpRequest();
  xhr.open('GET', this.archiveURL + '?path=' + encodeURIComponent(path), true);

  // Add event listeners
  xhr.addEventListener('load', handleLoad, false);
  xhr.addEventListener('error', handleError, false);

  // Set mime type
  if (xhr.overrideMimeType) {
    xhr.overrideMimeType('text/plain');
  }

  // Send request
  xhr.send(null);

  // Indicate loading of item
  item.className = item.className.replace('new', 'loading').replace('error', 'loading');

  // Remove load listener
  evt.target.removeEventListener('click', this.loadFolderListener, false);
  evt.target.removeEventListener('keydown', this.loadFolderListener, false);
};



/**
 * Action handler for loading a game log file.
 *
 * @param {!Event} evt the click event
 * @return {void}
 */
JaSMIn.UI.Archive.prototype.loadGameLog = function(evt) {
  if (!JaSMIn.UI.isButtonAction(evt)) {
    return;
  }

  if (this.gameLogSelectionCallback) {
    var path = evt.target.parentNode.dataset.path;
    var idx = this.archiveURL.lastIndexOf('/');

    this.gameLogSelectionCallback(this.archiveURL.slice(0, idx + 1) + path);
  }
};



/**
 * Action handler for loading a playlist file.
 *
 * @param {!Event} evt the click event
 * @return {void}
 */
JaSMIn.UI.Archive.prototype.loadPlaylist = function(evt) {
  if (!JaSMIn.UI.isButtonAction(evt)) {
    return;
  }

  if (this.playlistSelectionCallback) {
    var path = evt.target.parentNode.dataset.path;
    var idx = this.archiveURL.lastIndexOf('/');

    this.playlistSelectionCallback(this.archiveURL.slice(0, idx + 1) + path);
  }
};



/**
 * Toggle expanded state of the clicked item.
 *
 * @param {!Event} evt the click event
 * @return {void}
 */
JaSMIn.UI.Archive.toggleExpand = function(evt) {
  if (!JaSMIn.UI.isButtonAction(evt)) {
    return;
  }

  var item = evt.target.parentNode;

  if (JaSMIn.UI.toggleVisibility(item.getElementsByTagName('ul')[0])) {
    item.className = item.className.replace('expandable', 'expanded');
  } else {
    item.className = item.className.replace('expanded', 'expandable');
  }
};



/**
 * Toggle expanded state of the clicked item.
 *
 * @param {!Event} evt the click event
 * @return {void}
 */
JaSMIn.UI.Archive.removeArchive = function(evt) {
  if (!JaSMIn.UI.isButtonAction(evt)) {
    return;
  }

  // Remove dom node
  var item = evt.target.parentNode.parentNode;
  item.parentNode.removeChild(item);
};
