/**
 * The ArchiveExplorer class definition.
 *
 * The ArchiveExplorer provides browsing capabilities to multiple remote archives.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.ArchiveExplorer');

goog.require('JaSMIn.UI');
goog.require('JaSMIn.UI.Archive');



/**
 * ArchiveExplorer Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Panel}
 * @param {!JaSMIn.LogPlayer} logPlayer the log player model
 */
JaSMIn.UI.ArchiveExplorer = function(logPlayer) {
  goog.base(this, 'jsm-archive-explorer');

  /**
   * The log player model instance.
   * @type {!JaSMIn.LogPlayer}
   */
  this.logPlayer = logPlayer;


  /**
   * The archive list.
   * @type {!Element}
   */
  this.archiveList = JaSMIn.UI.createUL('archive-list');
  this.domElement.appendChild(this.archiveList);


  /**
   * The add archive list item.
   * @type {!Element}
   */
  this.addArchiveItem = JaSMIn.UI.createLI('add-archive expandable');
  this.archiveList.appendChild(this.addArchiveItem);

  var label = JaSMIn.UI.createSpan('Add new Archive',  'no-text-select');
  label.addEventListener('click', JaSMIn.UI.ArchiveExplorer.toggleExpand, false);
  this.addArchiveItem.appendChild(label);


  /**
   * The add archive list item.
   * @type {!Element}
   */
  this.addArchiveBox = JaSMIn.UI.createDiv('add-box');
  JaSMIn.UI.setVisibility(this.addArchiveBox, false);
  this.addArchiveItem.appendChild(this.addArchiveBox);


  /**
   * The new archive location input field.
   * @type {!Element}
   */
  this.archiveLocationInput = JaSMIn.UI.createElement('input');
  this.archiveLocationInput.name = 'location';
  this.archiveLocationInput.type = 'url';
  this.archiveLocationInput.value = 'https://';

  label = JaSMIn.UI.createElement('label');
  label.appendChild(JaSMIn.UI.createSpan('URL:'));
  label.appendChild(this.archiveLocationInput);
  this.addArchiveBox.appendChild(label);


  /**
   * The new archive name input field.
   * @type {!Element}
   */
  this.archiveNameInput = JaSMIn.UI.createElement('input');
  this.archiveNameInput.name = 'name';
  this.archiveNameInput.type = 'text';

  label = JaSMIn.UI.createElement('label');
  label.appendChild(JaSMIn.UI.createSpan('Name:'));
  label.appendChild(this.archiveNameInput);
  this.addArchiveBox.appendChild(label);

  /** @type {!Function} */
  this.onAddNewLocationListener = this.onAddNewLocation.bind(this);


  /**
   * The add new archive button.
   * @type {!Element}
   */
  this.addArchiveBtn = JaSMIn.UI.createButton('Add', 'add-archive', 'Add new archive location to list of archives', this.onAddNewLocationListener);
  this.addArchiveBox.appendChild(this.addArchiveBtn);


  /** @type {!Function} */
  this.handleGameLogSelectedListener = this.handleGameLogSelected.bind(this);
  /** @type {!Function} */
  this.handlePlaylistSelectedListener = this.handlePlaylistSelected.bind(this);
};
goog.inherits(JaSMIn.UI.ArchiveExplorer, JaSMIn.UI.Panel);



/**
 * Add location action listener.
 *
 * @param {!Event} evt the button event
 * @return {void}
 */
JaSMIn.UI.ArchiveExplorer.prototype.onAddNewLocation = function(evt) {
  var url = this.archiveLocationInput.value;
  var label = this.archiveNameInput.value;

  if (!url || url === 'https://' || url === 'http://') {
    return;
  }

  if (!label) {
    label = url;
  }

  // Add location
  this.addLocation(url, label);

  // Reset input elements
  this.archiveLocationInput.value = 'https://';
  this.archiveNameInput.value = '';

  // Hide input elements
  JaSMIn.UI.setVisibility(this.addArchiveBox, false);
  this.addArchiveItem.className = this.addArchiveItem.className.replace(' expanded', '');
};



/**
 * Add new location to list of archives.
 *
 * @param {!string} url the url to the new archive location
 * @param {!string} label the label text to display
 * @return {void}
 */
JaSMIn.UI.ArchiveExplorer.prototype.addLocation = function(url, label) {
  var newArchive = new JaSMIn.UI.Archive(url, label);
  newArchive.gameLogSelectionCallback = this.handleGameLogSelectedListener;
  newArchive.playlistSelectionCallback = this.handlePlaylistSelectedListener;
  this.archiveList.appendChild(newArchive.domElement);
};



/**
 * Handle selection of a game log within one of the archives.
 *
 * @param {!string} logURL the selected game log url
 * @return {void}
 */
JaSMIn.UI.ArchiveExplorer.prototype.handleGameLogSelected = function(logURL) {
  this.logPlayer.loadGameLog(logURL);
};



/**
 * Handle selection of a playlist within one of the archives.
 *
 * @param {!string} listURL the selected playlist url
 * @return {void}
 */
JaSMIn.UI.ArchiveExplorer.prototype.handlePlaylistSelected = function(listURL) {
  this.logPlayer.loadPlaylist(listURL);
};



/**
 * Toggle expanded state of the clicked item.
 *
 * @param {!Event} evt the click event
 * @return {void}
 */
JaSMIn.UI.ArchiveExplorer.toggleExpand = function(evt) {
  var item = evt.target.parentNode;

  if (JaSMIn.UI.toggleVisibility(item.getElementsByTagName('div')[0])) {
    item.className = item.className.replace('expandable', 'expanded');
  } else {
    item.className = item.className.replace('expanded', 'expandable');
  }
};
