goog.provide('JaSMIn.UI.GLInfoBoard');

goog.require('JaSMIn.FPSMeter');
goog.require('JaSMIn.UI');



/**
 * GLInfoBoard Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Panel}
 * @param {!JaSMIn.FPSMeter} fpsMeter the fps meter used by the gl panel
 */
JaSMIn.UI.GLInfoBoard = function(fpsMeter) {
  goog.base(this, 'jsm-gl-info no-text-select');

  /**
   * The FPS meter instance.
   * @type {!JaSMIn.FPSMeter}
   */
  this.fpsMeter = fpsMeter;
  this.fpsMeter.onNewSecond = this.handleNewSecond.bind(this);


  var list = document.createElement('ul');
  this.domElement.appendChild(list);

  /**
   * The FPS label.
   * @type {!Element}
   */
  this.fpsLbl = JaSMIn.UI.createSpan('0');

  var item = document.createElement('li');
  item.appendChild(JaSMIn.UI.createSpan('FPS:', 'label'));
  item.appendChild(this.fpsLbl);
  list.appendChild(item);

  /**
   * The reosultion label.
   * @type {!Element}
   */
  this.resolutionLbl = JaSMIn.UI.createSpan('0 x 0px');

  // item = document.createElement('li');
  // item.appendChild(JaSMIn.UI.createSpan('Resolution:', 'label'));
  // item.appendChild(this.resolutionLbl);
  // list.appendChild(item);
};
goog.inherits(JaSMIn.UI.GLInfoBoard, JaSMIn.UI.Panel);






/**
 * Set the fps label.
 *
 * @param {!number} fps the current fps
 */
JaSMIn.UI.GLInfoBoard.prototype.setFPS = function(fps) {
  this.fpsLbl.innerHTML = fps;
};



/**
 * Set the fps label.
 *
 * @param {!number} width the monitor width
 * @param {!number} height the monitor height
 */
JaSMIn.UI.GLInfoBoard.prototype.setResolution = function(width, height) {
  this.resolutionLbl.innerHTML = '' + width + ' x ' + height + 'px';
};



/**
 * FPSMeter->"onNewSecond" event listener.
 */
JaSMIn.UI.GLInfoBoard.prototype.handleNewSecond = function() {
  this.fpsLbl.innerHTML = this.fpsMeter.fpsHistory[0];
};
