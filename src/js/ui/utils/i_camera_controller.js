/**
 * The ICameraController interface definition.
 *
 * The ICameraController
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.ICameraController');

goog.require('JaSMIn.UI');



/**
 * ICameraController Constructor
 *
 * @interface
 */
JaSMIn.UI.ICameraController = function() {};





/**
 * Enable/Disable the camera controller.
 *
 * @param {!boolean} enabled true to enable the camera controller, false for disabling
 */
JaSMIn.UI.ICameraController.prototype.setEnabled = function(enabled) {};



/**
 * Set the bounds of the camera controller.
 *
 * @param  {!THREE.Vector3} bounds the new world bounds
 * @return {void}
 */
JaSMIn.UI.ICameraController.prototype.setBounds = function(bounds) {};



/**
 * Set the area of interest (+/- dimensions around the origin).
 *
 * @param  {!THREE.Vector2} areaOfInterest the area of interest
 * @return {void}
 */
JaSMIn.UI.ICameraController.prototype.setAreaOfInterest = function(areaOfInterest) {};



/**
 * Update the camera controller.
 * The update is needed for keyboard movements, as well for tracking objects.
 *
 * @param  {!number} deltaT the time since the last render call
 * @return {void}
 */
JaSMIn.UI.ICameraController.prototype.update = function(deltaT) {};
