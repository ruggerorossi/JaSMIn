/**
 * The TabPane class definition.
 *
 * The TabPane abstracts
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.Tab');
goog.provide('JaSMIn.UI.TabPane');

goog.require('JaSMIn.UI');
goog.require('JaSMIn.UI.Panel');
goog.require('JaSMIn.UI.PanelGroup');



/**
 * Tab Constructor
 *
 * @constructor
 * @param {!JaSMIn.UI.Panel} head the tab header panel
 * @param {!JaSMIn.UI.Panel} content the tab content panel
 */
JaSMIn.UI.Tab = function(head, content) {

  /**
   * The tab header panel.
   * @type {!JaSMIn.UI.Panel}
   */
  this.head = head;

  /**
   * The tab content panel.
   * @type {!JaSMIn.UI.Panel}
   */
  this.content = content;
};



/**
 * TabPane Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Panel}
 * @param {!string=} className the css class string
 */
JaSMIn.UI.TabPane = function(className) {
  goog.base(this, 'jsm-tab-pane' + (className === undefined ? '' : ' ' + className));

  // Create header row
  var row = JaSMIn.UI.createDiv('t-row');
  this.domElement.appendChild(row);

  var cell = JaSMIn.UI.createDiv('tab-header');
  row.appendChild(cell);


  /**
   * The tab header container.
   * @type {!Element}
   */
  this.tabHeaderList = JaSMIn.UI.createUL();
  cell.appendChild(this.tabHeaderList);

  // Create content row
  row = JaSMIn.UI.createDiv('t-row');
  this.domElement.appendChild(row);

  /**
   * The tab header container.
   * @type {!Element}
   */
  this.tabContent = JaSMIn.UI.createDiv('tab-content');
  row.appendChild(this.tabContent);

  /**
   * The tabs of this tab pane.
   * @type {!Array<!JaSMIn.UI.Tab>}
   */
  this.tabs = [];

  /**
   * The tab group, managing visibility of content panels.
   * @type {!JaSMIn.UI.PanelGroup}
   */
  this.tabGroup = new JaSMIn.UI.PanelGroup();
};
goog.inherits(JaSMIn.UI.TabPane, JaSMIn.UI.Panel);



/**
 * Add the given tab to the tab-pane.
 *
 * @param  {!JaSMIn.UI.Tab} tab the new tab to add
 */
JaSMIn.UI.TabPane.prototype.add = function(tab) {
  // Store new tab
  this.tabs.push(tab);

  // Add content to tab panel group
  this.tabGroup.add(tab.content);

  // Add new tab to containers
  var li = JaSMIn.UI.createLI();
  li.onclick = function (evt) {
    tab.content.setVisible();

    // Deactivate all header items
    var tabHeaders = li.parentNode.childNodes;
    for (var i = 0; i < tabHeaders.length; ++i) {
      if (tabHeaders[i].nodeName === 'LI') {
        tabHeaders[i].className = tabHeaders[i].className.replace('active', '');
      }
    }

    // Reactivate selected item
    li.className += ' active';
  }

  li.appendChild(tab.head.domElement);
  this.tabHeaderList.appendChild(li);
  this.tabContent.appendChild(tab.content.domElement);

  // Activate first tab
  if (this.tabs.length === 1) {
    // By default show first tab
    tab.content.setVisible();
    li.className = 'active';
  }
};



/**
 * Add the given panels as tab to the tab-pane.
 *
 * @param  {!JaSMIn.UI.Panel} head the tab header panel
 * @param  {!JaSMIn.UI.Panel} content the tab content panel
 * @return {!JaSMIn.UI.Tab} the newly created and added tab
 */
JaSMIn.UI.TabPane.prototype.addPanels = function(head, content) {
  var newTab = new JaSMIn.UI.Tab(head, content);

  this.add(newTab);

  return newTab;
};



/**
 * Wrap the given elements in panels and add them as tab to the tab-pane.
 *
 * @param  {!Element} head the tab header element
 * @param  {!Element} content the tab content element
 * @return {!JaSMIn.UI.Tab} the newly created and added tab
 */
JaSMIn.UI.TabPane.prototype.addElements = function(head, content) {
  var headPanel = new JaSMIn.UI.Panel();
  headPanel.appendChild(head);

  var contentPanel = new JaSMIn.UI.Panel();
  contentPanel.appendChild(content);

  return this.addPanels(headPanel, contentPanel);
};
