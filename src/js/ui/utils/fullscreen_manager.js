goog.provide('JaSMIn.UI.FullscreenManager');
goog.provide('JaSMIn.UI.FullscreenManagerEvents');

goog.require('JaSMIn.EventDispatcher');
goog.require('JaSMIn.UI');



/**
 * @enum {!string}
 */
JaSMIn.UI.FullscreenManagerEvents = {
  CHANGE: 'change',
};




/**
 * FullscreenManager Constructor.
 *
 * @constructor
 * @implements {JaSMIn.IPublisher}
 * @implements {JaSMIn.IEventDispatcher}
 * @param {!Element} container the container of interest
 */
JaSMIn.UI.FullscreenManager = function(container) {
  /**
   * The container of interest.
   * @type {!Element}
   */
  this.container = container;


  /**
   * The change event.
   * @type {!Object}
   */
  this.changeEvent = { type: JaSMIn.UI.FullscreenManagerEvents.CHANGE };


  /** @type {!Function} */
  this.onFullscreenChangeListener = this.onFullscreenChange.bind(this);


  // Add fullscreen change listeners
  document.addEventListener('fullscreenchange', this.onFullscreenChangeListener);
  document.addEventListener('mozfullscreenchange', this.onFullscreenChangeListener);
  document.addEventListener('msfullscreenchange', this.onFullscreenChangeListener);
  document.addEventListener('webkitfullscreenchange', this.onFullscreenChangeListener);
};



/**
 * Toggle fullscreen mode of container.
 */
JaSMIn.UI.FullscreenManager.prototype.toggleFullscreen = function() {
  if (this.container === JaSMIn.UI.getFullscreenElement()) {
    JaSMIn.UI.cancelFullscreen();
  } else {
    JaSMIn.UI.requestFullscreenFor(this.container);
  }

  // Publish change event
  this.dispatchEvent(this.changeEvent);
};



/**
 * Request fullscreen mode for container.
 */
JaSMIn.UI.FullscreenManager.prototype.requestFullscreen = function() {
  if (this.container !== JaSMIn.UI.getFullscreenElement()) {
    JaSMIn.UI.requestFullscreenFor(this.container);

    // Publish change event
    this.dispatchEvent(this.changeEvent);
  }
};



/**
 * Cancel fullscreen mode for container.
 */
JaSMIn.UI.FullscreenManager.prototype.cancelFullscreen = function() {
  if (this.container === JaSMIn.UI.getFullscreenElement()) {
    JaSMIn.UI.cancelFullscreen();

    // Publish change event
    this.dispatchEvent(this.changeEvent);
  }
};



/**
 * Check if the container is currently in fullscreen mode.
 */
JaSMIn.UI.FullscreenManager.prototype.isFullscreen = function() {
  return this.container === JaSMIn.UI.getFullscreenElement();
};



/**
 * The callback triggered when the window enters / leaves fullscreen.
 *
 * @param  {!Object} event the event
 * @return {void}
 */
JaSMIn.UI.FullscreenManager.prototype.onFullscreenChange = function(event) {
  // Publish change event
  this.dispatchEvent(this.changeEvent);
};





// ============================== EVENT DISPATCHER FUNCTIONS ==============================
/** @override */
JaSMIn.UI.FullscreenManager.prototype.addEventListener = JaSMIn.EventDispatcher.prototype.addEventListener;
/** @override */
JaSMIn.UI.FullscreenManager.prototype.removeEventListener = JaSMIn.EventDispatcher.prototype.removeEventListener;
/** @override */
JaSMIn.UI.FullscreenManager.prototype.dispatchEvent = JaSMIn.EventDispatcher.prototype.dispatchEvent;
// ============================== EVENT DISPATCHER FUNCTIONS ==============================
