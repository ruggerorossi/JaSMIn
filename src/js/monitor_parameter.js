goog.provide('JaSMIn.MonitorParameter');

goog.require('JaSMIn');




/**
 * MonitorParameter Constructor
 *
 * @constructor
 * @param {Object=} params the external monitor parameter object
 */
JaSMIn.MonitorParameter = function(params) {

  /**
   * The monitor parameter object.
   * @type {!Object}
   */
  this.monitorParams = (params !== undefined && params !== null) ? params : {};

  /**
   * The query parameter object.
   * @type {!Object}
   */
  this.queryParams = JaSMIn.getQueryParams();
};



/**
 * Retrieve a query parameter.
 *
 * @param  {!string} key the parameter key
 * @return {?string} the query parameter if specified, or null otherwise
 */
JaSMIn.MonitorParameter.prototype.getQueryParam = function(key) {
  if (this.queryParams[key]) {
    return this.queryParams[key];
  }

  return null;
};



/**
 * Check for embedded parameter.
 *
 * @return {!boolean} true, if embedded mode is set and true, false otherwise
 */
JaSMIn.MonitorParameter.prototype.isEmbedded = function() {
  return this.monitorParams['embedded'] === true;
};



/**
 * Retrieve archives parameter.
 *
 * @return {!Array<{url: string, name: string}>} the list of predefined archive locations
 */
JaSMIn.MonitorParameter.prototype.getArchives = function() {
  if (this.monitorParams['archives']) {
    return this.monitorParams['archives'];
  }

  return [];
};



/**
 * Retrieve the game log / replay url parameter.
 *
 * @return {?string} the game log url if specified, or null otherwise
 */
JaSMIn.MonitorParameter.prototype.getGameLogURL = function() {
  var url = this.getQueryParam('gamelog');

  if (url === null) {
    // Alternatively check for "replay" parameter
    url = this.getQueryParam('replay');
  }

  return url;
};



/**
 * Retrieve the playlist url parameter.
 *
 * @return {?string} the playlist url if specified, or null otherwise
 */
JaSMIn.MonitorParameter.prototype.getPlaylistURL = function() {
  return this.getQueryParam('list');
};
