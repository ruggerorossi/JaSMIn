goog.provide('JaSMIn');



/** General definitions, namespaces, version, etc.
  *
  * @author Stefan Glaser
  */
var JaSMIn = { REVISION: '0.2' };



/**
 * Enum holding the known game types.
 * @enum {!number}
 */
JaSMIn.GameType = {
  TWOD: 1,
  THREED: 2
};



/**
 * An enum providing parameter types.
 * @enum {!number}
 */
JaSMIn.ParameterType = {
  STRING: 0,
  FLOAT: 1,
  INT: 2,
  BOOL: 3
};



/**
 * An enum for the side of a team.
 * @enum {!number}
 */
JaSMIn.TeamSide = {
  LEFT: -1,
  NEUTRAL: 0,
  RIGHT: 1
};



/**
 * An enum for possible data extents.
 * @enum {!number}
 */
JaSMIn.DataExtent = {
  COMPLETE: 0,
  PARTIAL: 1,
  INCREMENTAL: 2
};



/**
 * Retrieve a letter representing the side.
 *
 * @param  {!JaSMIn.TeamSide} side the side value
 * @param  {!boolean=} uppercase true for upper case letter, false for lower case
 * @return {!string} 'l'/'L' for left side, 'r'/'R' for right side, 'n'/'N' for neutral
 */
JaSMIn.getSideLetter = function(side, uppercase) {
  if (uppercase) {
    return side === JaSMIn.TeamSide.LEFT ? 'L' : side === JaSMIn.TeamSide.RIGHT ? 'R' : 'N';
  } else {
    return side === JaSMIn.TeamSide.LEFT ? 'l' : side === JaSMIn.TeamSide.RIGHT ? 'r' : 'n';
  }
};



/**
 * @param  {!string} rawValue the row value
 * @param  {!JaSMIn.ParameterType} type the parameter type
 * @return {!string | !number | !boolean | undefined}
 */
JaSMIn.parseParamValue = function(rawValue, type) {
  if (rawValue === undefined) {
    return undefined;
  }

  switch (type) {
    case JaSMIn.ParameterType.FLOAT:
      return parseFloat(rawValue);
      break;
    case JaSMIn.ParameterType.INT:
      return parseInt(rawValue, 10);
      break;
    case JaSMIn.ParameterType.BOOL:
      return rawValue === 'true';
      break;
    case JaSMIn.ParameterType.STRING:
    default:
      return JaSMIn.copyString(rawValue);
      break;
  }
};



/**
 * Constant for transforming degree to radian angles.
 * @const {!number}
 */
JaSMIn.PIby180 = Math.PI / 180;



/**
 * Constant for transforming degree to radian angles and negating the sign.
 * @const {!number}
 */
JaSMIn.NegPIby180 = Math.PI / -180;

/**
 * Transform degrees to radians.
 * @param {!number} deg
 * @return {!number}
 */
JaSMIn.toRad = function(deg) {
  return deg * Math.PI / 180;
};

/**
 * Transform radians to degrees.
 * @param {!number} rad
 * @return {!number}
 */
JaSMIn.toDeg = function(rad) {
  return rad * 180 / Math.PI;
};



/**
 * A THREE.Vector3 instance representing the zero vector (0, 0, 0).
 * @const {!THREE.Vector3}
 */
JaSMIn.Vector3_zero = new THREE.Vector3(0, 0, 0);

/**
 * A THREE.Vector3 instance representing the X unit vector (1, 0, 0).
 * @const {!THREE.Vector3}
 */
JaSMIn.Vector3_unitX = new THREE.Vector3(1, 0, 0);

/**
 * A THREE.Vector3 instance representing the Y unit vector (0, 1, 0).
 * @const {!THREE.Vector3}
 */
JaSMIn.Vector3_unitY = new THREE.Vector3(0, 1, 0);

/**
 * A THREE.Vector3 instance representing the Z unit vector (0, 0, 1).
 * @const {!THREE.Vector3}
 */
JaSMIn.Vector3_unitZ = new THREE.Vector3(0, 0, 1);



/**
 * A white threejs color.
 * @type {!THREE.Color}
 */
JaSMIn.Color_White = new THREE.Color(0xffffff);

/**
 * A black threejs color.
 * @type {!THREE.Color}
 */
JaSMIn.Color_Black = new THREE.Color(0x000000);

/**
 * A threejs color with color value #eeeeee.
 * @type {!THREE.Color}
 */
JaSMIn.Color_eee = new THREE.Color(0xeeeeee);

/**
 * A threejs color with color value #333333.
 * @type {!THREE.Color}
 */
JaSMIn.Color_333 = new THREE.Color(0x333333);



/**
 * Make Matrtix 4x4.
 *
 * @param  {!number} n11
 * @param  {!number} n12
 * @param  {!number} n13
 * @param  {!number} n14
 * @param  {!number} n21
 * @param  {!number} n22
 * @param  {!number} n23
 * @param  {!number} n24
 * @param  {!number} n31
 * @param  {!number} n32
 * @param  {!number} n33
 * @param  {!number} n34
 * @return {!THREE.Matrix4} a 4x4 matrix
 */
JaSMIn.mM4 = function(n11, n12, n13, n14, n21, n22, n23, n24, n31, n32, n33, n34) {
  return new THREE.Matrix4().set(n11, n12, n13, n14,
                                 n21, n22, n23, n24,
                                 n31, n32, n33, n34,
                                 0, 0, 0, 1);
};




/**
 * Key code enum.
 * @enum {!number}
 */
JaSMIn.KeyCodes = {
  ENTER: 13,
  SPACE: 32,

  PAGE_UP: 33,
  PAGE_DOWN: 34,
  LEFT: 37,
  UP: 38,
  RIGHT: 39,
  DOWN: 40,

  ZERO: 48,
  ONE: 49,
  TWO: 50,
  THREE: 51,
  FOUR: 52,
  FIVE: 53,
  SIX: 54,
  SEVEN: 55,
  EIGHT: 56,
  NINE: 57,

  A: 65,
  B: 66,
  C: 67,
  D: 68,
  E: 69,
  F: 70,
  G: 71,
  H: 72,
  I: 73,
  J: 74,
  K: 75,
  L: 76,
  M: 77,
  N: 78,
  O: 79,
  P: 80,
  Q: 81,
  R: 82,
  S: 83,
  T: 83,
  U: 85,
  V: 86,
  W: 87,
  X: 88,
  Y: 89,
  Z: 90
};




/**
 * Character code enum.
 * @enum {!number}
 */
JaSMIn.CharCodes = {
  BACKSPACE: 8,
  TAB: 9,
  ENTER: 13,
  ESC: 27,
  SPACE: 32,

  LP: 40,
  RP: 41,
  ASTERISK: 42,
  PLUS: 43,
  COMMA: 44,
  MINUS: 45,
  DOT: 46,
  SLASH: 47,

  ZERO: 48,
  ONE: 49,
  TWO: 50,
  THREE: 51,
  FOUR: 52,
  FIVE: 53,
  SIX: 54,
  SEVEN: 55,
  EIGHT: 56,
  NINE: 57,

  COLON: 58,
  SEMICOLON: 59,
  LT: 60,
  EQ: 61,
  GT: 62,
  QESTIONMARK: 63,
  AT: 64,

  A: 65,
  B: 66,
  C: 67,
  D: 68,
  E: 69,
  F: 70,
  G: 71,
  H: 72,
  I: 73,
  J: 74,
  K: 75,
  L: 76,
  M: 77,
  N: 78,
  O: 79,
  P: 80,
  Q: 81,
  R: 82,
  S: 83,
  T: 83,
  U: 85,
  V: 86,
  W: 87,
  X: 88,
  Y: 89,
  Z: 90,

  LSB: 91,
  BACKSLASH: 92,
  RSB: 93,
  CIRCUMFLEX: 94,
  LOWLINE: 95,
  GRAVEACCENT: 96,

  a: 97,
  b: 98,
  c: 99,
  d: 100,
  e: 101,
  f: 102,
  g: 103,
  h: 104,
  i: 105,
  j: 106,
  k: 107,
  l: 108,
  m: 109,
  n: 110,
  o: 111,
  p: 112,
  q: 113,
  r: 114,
  s: 115,
  t: 116,
  u: 117,
  v: 118,
  w: 119,
  x: 120,
  y: 121,
  z: 122,

  LCB: 123,
  VBAR: 124,
  RCB: 125,
  TILDE: 126,
  DEL: 127
};



/**
 * The copyString funcion presents a workaround for deep copying partial strings.
 *
 * Modern browsers only provide partial strings when using string.substring() / .slice() / etc.
 * while keeping a reference to the original string. While this usually improves the overall
 * performance and memory consumption, it also prevents the garbage collector from collecting
 * the original string. This function provides a workaround for really copying a string value
 * (obtained via .substring() / .slice() / etc.).
 * Use this function when storing partial strings in your result objects.
 *
 * @param  {!string} partialString
 * @return {!string} a "deep" copy of the above partial string
 */
JaSMIn.copyString = function(partialString) {
  if (partialString) {
    return JSON.stringify(partialString).slice(1, -1);
  }

  return partialString;
};




/**
 * Extract the event position relative to the given element.
 *
 * @param  {!Element} element the parent element
 * @param  {!Event} event the event
 * @return {!THREE.Vector2}
 */
JaSMIn.eventToLocalPos = function(element, event) {
  var rect = element.getBoundingClientRect();

  return new THREE.Vector2(event.clientX - rect.left, event.clientY - rect.top);
};


/**
 * Extract the event position relative to the center of the given element.
 *
 * @param  {!Element} element the parent element
 * @param  {!Event} event the event
 * @return {!THREE.Vector2}
 */
JaSMIn.eventToLocalCenterPos = function(element, event) {
  var halfWidth = element.clientWidth / 2;
  var halfHeight = element.clientHeight / 2;
  var rect = element.getBoundingClientRect();

  return new THREE.Vector2(event.clientX - rect.left - halfWidth, halfHeight - event.clientY + rect.top);
};



/**
 * Extract the file name from an url string.
 *
 * @param  {!string} url the url to extract the file name from
 * @return {!string} the file name or the given url if the url doesn't contain any subfolders
 */
JaSMIn.getFileName = function(url) {
  var endIdx = url.indexOf('?');
  if (endIdx === -1) {
    // No parameter indication character found
    endIdx = url.length;
  }

  var startIdx = url.slice(0, endIdx).lastIndexOf('/');

  return url.slice(startIdx + 1, endIdx);
};



/**
 * Extract the file type from an url string.
 *
 * @param  {!string} url the url to extract the file type from
 * @return {?string} the file type or null if the path doesn't refer a file
 */
JaSMIn.getFileType = function(url) {
  // TODO: Find a proper solution...
  var lastDotIdx = url.lastIndexOf('.');

  if (lastDotIdx !== -1) {
    return url.slice(lastDotIdx + 1);
  } else {
    return null;
  }
};



/**
 * Filter a list of files according to their name suffixes.
 *
 * @param  {!Array<!File>} files a list of files
 * @param  {!Array<!string>} suffixes the list of suffixes to filter for
 * @return {!Array<!File>} a list of files with the given suffixes
 */
JaSMIn.filterFiles = function(files, suffixes) {
  var filteredFiles = [];

  for (var i = 0; i < files.length; i++) {
    for (var j = 0; j < suffixes.length; j++) {
      if (files[i].name.slice(-suffixes[j].length) === suffixes[j]) {
        filteredFiles.push(files[i]);
        break;
      }
    }
  }

  return filteredFiles;
};



/**
 * Check if the given url/path/file references a known replay file ending.
 *
 * @param  {!string} url the url to check
 * @param  {!boolean=} gzipAllowed indicator if gzipped versions are accepted
 * @return {!boolean} true, if the given url references a known replay file ending, false otherwise
 */
JaSMIn.isReplayFile = function(url, gzipAllowed) {
  var fileName = JaSMIn.getFileName(url);
  var suffix9 = fileName.slice(-9);
  var suffix6 = fileName.slice(-6);

  if (suffix6 === '.rpl3d' || suffix6 === '.rpl2d' || fileName.slice(-7) === '.replay') {
    return true;
  } else if (gzipAllowed && (suffix9 === '.rpl3d.gz' || suffix9 === '.rpl2d.gz' || fileName.slice(-10) === '.replay.gz')) {
    return true;
  }

  return false;
};



/**
 * Check if the given url/path/file references a known sserver log file ending.
 *
 * @param  {!string} url the url to check
 * @param  {!boolean=} gzipAllowed indicator if gzipped file versions are accepted
 * @return {!boolean} true, if the given url references a known sserver log file ending, false otherwise
 */
JaSMIn.isSServerLogFile = function(url, gzipAllowed) {
  var fileName = JaSMIn.getFileName(url);

  return fileName.slice(-4) === '.rcg' || (gzipAllowed !== undefined && fileName.slice(-7) === '.rcg.gz');
};



/**
 * Extract the query parameters from a query string or the current location.
 *
 * @export
 * @param  {!string=} query the query string to parse or undefined for window.location.search
 * @return {!Object<string,string>} the query parameter map
 */
JaSMIn.getQueryParams = function(query) {
  if (query === undefined) {
    query = window.location.search;
  }

  var params = {};
  var tokens;
  var regex = /[?&]?([^=]+)=([^&]*)/g;

  query = query.split('+').join(' ');

  while (tokens = regex.exec(query)) {
    params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
  }

  return params;
}









// ============================== THREEJS SETTINGS ==============================
/** Turn off autoupdate of object matrices */
THREE.Object3D.DefaultMatrixAutoUpdate = true;
// ============================== THREEJS SETTINGS ==============================
