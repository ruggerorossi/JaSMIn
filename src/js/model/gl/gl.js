goog.provide('JaSMIn.GL');

goog.require('JaSMIn');




/**
 * General web-gl namespace, definitions, etc.
 *
 * @author Stefan Glaser
 */
JaSMIn.GL = {};



/**
 * Create a geometry representing the field lines.
 *
 * @param  {!number} lineWidth the field line width
 * @param  {!THREE.Vector2} fieldDimensions the dimensions of the field
 * @param  {!number} centerRadius the radius of the center circle
 * @param  {!THREE.Vector2} goalAreaDimensions the dimensions of the goal area
 * @param  {?THREE.Vector3} penaltyAreaDimensions the dimensions of the penalty area + penalty kick spot
 * @return {!THREE.BufferGeometry}
 */
JaSMIn.GL.createFieldLinesGeometry = function(lineWidth,
                                              fieldDimensions,
                                              centerRadius,
                                              goalAreaDimensions,
                                              penaltyAreaDimensions) {

  var halfLength = fieldDimensions.x / 2;
  var halfWidth = fieldDimensions.y / 2;
  var halfLineWidth = lineWidth / 2;
  var halfGoalAreaWidth = goalAreaDimensions.y / 2;
  var tempX = 0;
  var mat = new THREE.Matrix4();

  /**
   * Helper function for simple merging of geometries.
   *
   * @param {!number} x the x position value
   * @param {!number} y the y position value
   * @param {!number=} rotZ the z rotation (if not zero)
   */
  var mergeAt = function (x, y, rotZ) {
    // Set matrix rotation
    if (rotZ) {
      mat.makeRotationZ(rotZ);
    } else {
      mat.identity();
    }

    // Set matrix position
    mat.elements[12] = x;
    mat.elements[13] = y;

    // Merge geometry
    totalGeometry.merge(tempGeometry, mat);
  };


  // ---------- Center circle geometry
  var radius = centerRadius;
  var totalGeometry = new THREE.RingGeometry(radius - halfLineWidth, radius + halfLineWidth, 64, 1);


  // ---------- Vertical field line geometry
  var tempGeometry = new THREE.PlaneGeometry(lineWidth, fieldDimensions.y);

  // Left/Right border line
  mergeAt(-halfLength, 0);
  mergeAt(halfLength, 0);

  // Center line
  mergeAt(0, 0);


  // ---------- Horizontal field line geometry
  tempGeometry = new THREE.PlaneGeometry(fieldDimensions.x + lineWidth, lineWidth);

  // Top/Bottom border line
  mergeAt(0, -halfWidth);
  mergeAt(0, halfWidth);


  // ---------- Corner circle geometry
  radius = fieldDimensions.x / 105.0;
  tempGeometry = new THREE.RingGeometry(radius - halfLineWidth, radius + halfLineWidth, 8, 1, 0, Math.PI / 2);

  // Top left corner circle
  mergeAt(-halfLength, -halfWidth);

  // Top right corner circle
  mergeAt(halfLength, -halfWidth, Math.PI / 2);

  // Bottom right corner circle
  mergeAt(halfLength, halfWidth, Math.PI);

  // Bottom left corner circle
  mergeAt(-halfLength, halfWidth, -Math.PI / 2);


  // ---------- Center spot geometry
  tempGeometry = new THREE.CircleGeometry(lineWidth * 1.2, 16);
  mergeAt(0, 0);


  // Penalty area
  if (penaltyAreaDimensions !== null) {
    var halfPenaltyWidth = penaltyAreaDimensions.y / 2;
    tempX = halfLength - penaltyAreaDimensions.z;

    // Left/Right penalty kick spot
    mergeAt(-tempX, 0);
    mergeAt(tempX, 0);


    // ---------- Vertical penalty area line geometry
    tempGeometry = new THREE.PlaneGeometry(lineWidth, penaltyAreaDimensions.y + lineWidth);
    tempX = halfLength - penaltyAreaDimensions.x;

    // Left/Right penalty area front line
    mergeAt(-tempX, 0);
    mergeAt(tempX, 0);


    // ---------- Horizontal penalty area line geometry
    tempGeometry = new THREE.PlaneGeometry(penaltyAreaDimensions.x, lineWidth);
    tempX = halfLength - penaltyAreaDimensions.x / 2;

    // Left/Right penalty area top line
    mergeAt(-tempX, -halfPenaltyWidth);
    mergeAt(tempX, -halfPenaltyWidth);

    // Left/Right penalty area bottom line
    mergeAt(-tempX, halfPenaltyWidth);
    mergeAt(tempX, halfPenaltyWidth);


    // ---------- Penalty area arcs geometry
    var diffAngle = Math.acos((penaltyAreaDimensions.x - penaltyAreaDimensions.z) / centerRadius);
    tempGeometry = new THREE.RingGeometry(centerRadius - halfLineWidth, centerRadius + halfLineWidth, 32, 1, diffAngle, -2 * diffAngle);
    tempX = halfLength - penaltyAreaDimensions.z;

    // Left/Right penalty area arc
    mergeAt(-tempX, 0);
    mergeAt(tempX, 0, Math.PI);
  }


  // ---------- Vertical goal area lines geometry
  tempGeometry = new THREE.PlaneGeometry(lineWidth, goalAreaDimensions.y + lineWidth);
  tempX = halfLength - goalAreaDimensions.x;

  // Left/Right goal area front line
  mergeAt(-tempX, 0);
  mergeAt(tempX, 0);


  // ---------- Horizontal goal area lines geometry
  tempGeometry = new THREE.PlaneGeometry(goalAreaDimensions.x, lineWidth);
  tempX = halfLength - goalAreaDimensions.x / 2;

  // Left/Right goal area top line
  mergeAt(-tempX, -halfGoalAreaWidth);
  mergeAt(tempX, -halfGoalAreaWidth);

  // Left/Right goal area bottom line
  mergeAt(-tempX, halfGoalAreaWidth);
  mergeAt(tempX, halfGoalAreaWidth);


  // Create final buffer geometry from total geometry
  var geometry = new THREE.BufferGeometry();
  geometry.name = 'fieldLinesGeo';
  geometry.fromGeometry(totalGeometry);

  return geometry;
};



/**
 * Create a geometry representing a hockey goal.
 *
 * @param  {!number} postRadius the goal post radius
 * @param  {!THREE.Vector3} dimensions the dimensions of the goal
 * @return {!THREE.BufferGeometry}
 */
JaSMIn.GL.createHockeyGoalGeometry = function(postRadius, dimensions) {
  var mat = new THREE.Matrix4();

  /**
   * Helper function for simple merging of geometries.
   *
   * @param {!number} x the x position value
   * @param {!number} y the y position value
   * @param {!number} z the z position value
   * @param {!number=} rot the x/y rotation (if not zero)
   * @param {!boolean=} yRot indicator if rot is about y
   */
  var mergeAt = function (x, y, z, rot, yRot) {
    // Set matrix rotation
    if (rot) {
      if (yRot) {
        mat.makeRotationY(rot);
      } else {
        mat.makeRotationX(rot);
      }
    } else {
      mat.identity();
    }

    // Set matrix position
    mat.elements[12] = x;
    mat.elements[13] = y;
    mat.elements[14] = z;

    // Merge geometry
    totalGeometry.merge(tempGeometry, mat);
  };


  var goalBarRadius = postRadius / 2;
  var halfGoalWidth = postRadius + dimensions.y / 2;
  var halfGoalHeight = (goalBarRadius + dimensions.z) / 2;

  var totalGeometry = new THREE.Geometry();


  // ---------- Goal post geometry
  var tempGeometry = new THREE.CylinderGeometry(postRadius, postRadius, dimensions.z + goalBarRadius, 16);

  // Left/Right goal posts
  mergeAt(postRadius, halfGoalWidth, halfGoalHeight, -Math.PI / 2);
  mergeAt(postRadius, -halfGoalWidth, halfGoalHeight, -Math.PI / 2);


  // ---------- Upper goal bar geometry
  tempGeometry = new THREE.CylinderGeometry(goalBarRadius, goalBarRadius, halfGoalWidth * 2, 8);

  // Upper goal bar
  mergeAt(postRadius, 0, dimensions.z);


  // ---------- Bottom goal bar cylinder geometry
  var angle = Math.atan(0.5 * dimensions.z / dimensions.x);
  tempGeometry = new THREE.CylinderGeometry(goalBarRadius, goalBarRadius, halfGoalWidth * 2, 8, 1, false, -0.5 * Math.PI, angle);

  // Bottom goal bar cylinder
  mergeAt(dimensions.x, 0, 0);


  // ---------- Bottom goal bar plane geometry
  tempGeometry = new THREE.PlaneGeometry(goalBarRadius, halfGoalWidth * 2);

  // Bottom goal bar bottom plane
  mergeAt(dimensions.x - goalBarRadius / 2, 0, 0);

  // Bottom goal bar upper plane
  mergeAt(dimensions.x - Math.cos(angle) * goalBarRadius / 2, 0, Math.sin(angle) * goalBarRadius / 2, angle, true);


  // ---------- Goal stand geometry
  var triShape = new THREE.Shape();
  triShape.moveTo(0, 0);
  triShape.lineTo(dimensions.x, 0);
  triShape.lineTo(0, dimensions.z / 2);
  triShape.lineTo(0, 0);
  tempGeometry = new THREE.ShapeGeometry(triShape);

  // Left/Right goal stands
  mergeAt(0, halfGoalWidth, 0, Math.PI / 2);
  mergeAt(0, -halfGoalWidth, 0, Math.PI / 2);


  // Create final buffer geometry from total geometry
  var geometry = new THREE.BufferGeometry();
  geometry.name = 'goalGeo';
  geometry.fromGeometry(totalGeometry);

  return geometry;
};



/**
 * Create a geometry representing the side nets of a hockey goal.
 *
 * @return {!THREE.BufferGeometry}
 */
JaSMIn.GL.createHockeyGoalSideNetGeometry = function() {
  var totalGeometry = new THREE.Geometry();

  var triShape = new THREE.Shape();
  triShape.moveTo(0, 0);
  triShape.lineTo(1, 0);
  triShape.lineTo(0, 1);
  triShape.lineTo(0, 0);
  var tempGeometry = new THREE.ShapeGeometry(triShape);

  var mat = new THREE.Matrix4();
  mat.makeRotationX(Math.PI / 2);
  mat.elements[13] = 0.5;
  totalGeometry.merge(tempGeometry, mat);
  mat.elements[13] = -0.5;
  totalGeometry.merge(tempGeometry, mat);


  // Create final buffer geometry from total geometry
  var geometry = new THREE.BufferGeometry();
  geometry.name = 'goalNetSidesGeo';
  geometry.fromGeometry(totalGeometry);

  return geometry;
};
