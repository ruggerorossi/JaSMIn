/**
 * The RobotModel class definition.
 *
 * The RobotModel provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.RobotModel');



/**
 * RobotModel Constructor
 *
 * @constructor
 * @param {!string} name the name of the agent model
 */
JaSMIn.RobotModel = function(name) {
  /**
   * The name of the robot model
   * @type {!string}
   */
  this.name = name;

  /**
   * The robot object group
   * @type {!THREE.Object3D}
   */
  this.objGroup = new THREE.Object3D();
  this.objGroup.name = name;
  this.objGroup.visible = false;

  /**
   * A list of Object3D objects representing the joints
   * @type {!Array<!THREE.Object3D>}
   */
  this.jointGroups = [];

  /**
   * The list of team materials of this robot model
   * @type {!Array<!THREE.Material>}
   */
  this.teamMatList = [];
};



/**
 * Check if this robot model is valid
 * @return {!boolean} true if the robot model is valid, false otherwise
 */
JaSMIn.RobotModel.prototype.isValid = function() {
  return this.objGroup.children.length > 0;
};



/**
 * Set visibility of this robot models' objects.
 * @param {!boolean} active true for visible, false for invisible
 * @return {void}
 */
JaSMIn.RobotModel.prototype.setActive = function(active) {
  if (this.objGroup.visible !== active) {
    this.objGroup.visible = active;
  }
};



/**
 * Check visibility of this robot models' objects.
 * @return {!boolean} true for visible, false otherwise
 */
JaSMIn.RobotModel.prototype.isActive = function() {
  return this.objGroup.visible;
};



/**
 * Update the joint objects and object settings according to the given angles and agent data.
 *
 * @param  {!Array<number>} angles the angles of the current state
 * @param  {!Array<number>} data the agent data of the current state
 * @param  {Array<number>=} nextAngles the angles of the next state
 * @param  {Array<number>=} nextData the agent data of the next state
 * @param  {!number=} t the interpolation time
 * @return {void}
 */
JaSMIn.RobotModel.prototype.update = function(angles, data, nextAngles, nextData, t) {
  var jointData = angles;
  var i;

  // Check if we need to interpolate
  if (nextAngles !== undefined && t > 0) {
    if (t >= 1) {
      jointData = nextAngles;
    } else {
      // Interpolate state variables
      jointData = [];
      i = Math.min(angles.length, nextAngles.length);

      while (i--) {
        jointData[i] = t * (nextAngles[i] - angles[i]) + angles[i];
      }
    }
  }

  // Apply joint angles to model
  i = Math.min(jointData.length, this.jointGroups.length);

  while (i--) {
    // Calculate quaternion from axis and angle
    this.jointGroups[i].setRotationFromAxisAngle(this.jointGroups[i].jointAxis, jointData[i]);
    this.jointGroups[i].updateMatrix();
  }

  // Call model data update
  this.updateData(data, nextData, t);
};



/**
 * Update agent specific settings based on agent data.
 *
 * @param  {!Array<(!number | undefined)>} data the agent data of the current state
 * @param  {Array<(!number | undefined)>=} nextData the agent data of the next state
 * @param  {!number=} t the interpolation time
 * @return {void}
 */
JaSMIn.RobotModel.prototype.updateData = function(data, nextData, t) {
  // Does intentionally nothing...
};



/**
 * Set the team color of this robot model
 *
 * @param {THREE.Color} color the new team color
 */
JaSMIn.RobotModel.prototype.setTeamColor = function(color) {
  var i = this.teamMatList.length;

  while (i--) {
    var mat = this.teamMatList[i];
    mat.color.copy(color);
    mat.needsUpdate = true;
  }
};
