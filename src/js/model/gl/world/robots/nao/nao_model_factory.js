goog.provide('JaSMIn.NaoModelFactory');

goog.require('JaSMIn.DynamicRobotModel');
goog.require('JaSMIn.MeshFactory');
goog.require('JaSMIn.NaoSpecification');



/**
 * NaoModelFactory Constructor
 *
 * @constructor
 * @implements {JaSMIn.RobotModelFactory}
 */
JaSMIn.NaoModelFactory = function() {
  /**
   * The mesh factory.
   * @type {!JaSMIn.MeshFactory}
   */
  this.meshFactory = new JaSMIn.MeshFactory(new JaSMIn.JSONGeometryFactory('models/nao_resources_v4.json'), new JaSMIn.NaoMaterialFactory());
};


/**
 * @override
 */
JaSMIn.NaoModelFactory.prototype.createModel = function(playerType, side, playerNo, environmentParams, playerParams) {
  var modelName = playerType.getString(JaSMIn.D3.PlayerTypeParams.MODEL_NAME);

  if (modelName !== null && modelName.slice(0, 3) === 'nao') {
    var modelType = playerType.getNumber(JaSMIn.D3.PlayerTypeParams.MODEL_TYPE);

    if (modelType === null) {
      modelType = 0;
    }

    var specification = new JaSMIn.NaoSpecification(side, modelType, playerNo);
    return new JaSMIn.DynamicRobotModel('nao_hetero', specification, this.meshFactory);
  }

  return null;
};


/**
 * @override
 */
JaSMIn.NaoModelFactory.prototype.dispose = function() {

};
