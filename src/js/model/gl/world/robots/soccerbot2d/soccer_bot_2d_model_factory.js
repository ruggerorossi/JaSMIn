goog.provide('JaSMIn.SoccerBot2DModelFactory');

goog.require('JaSMIn.MeshFactory');
goog.require('JaSMIn.SoccerBot2D');
goog.require('JaSMIn.SoccerBot2DSpecification');



/**
 * SoccerBot2DModelFactory Constructor
 *
 * @constructor
 * @implements {JaSMIn.RobotModelFactory}
 */
JaSMIn.SoccerBot2DModelFactory = function() {
  /**
   * The mesh factory.
   * @type {!JaSMIn.MeshFactory}
   */
  this.meshFactory = new JaSMIn.MeshFactory(new JaSMIn.SoccerBot2DGeometryFactory(), new JaSMIn.SoccerBot2DMaterialFactory());
};


/**
 * @override
 */
JaSMIn.SoccerBot2DModelFactory.prototype.createModel = function(playerType, side, playerNo, environmentParams, playerParams) {
  var robotSpec = new JaSMIn.SoccerBot2DSpecification(side, playerNo);
  return new JaSMIn.SoccerBot2D('SoccerBot2D', robotSpec, this.meshFactory, environmentParams);
};


/**
 * @override
 */
JaSMIn.SoccerBot2DModelFactory.prototype.dispose = function() {

};
