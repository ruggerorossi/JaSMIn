/**
 * The SoccerBot2D class definition.
 *
 * The SoccerBot2D provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.SoccerBot2D');

goog.require('JaSMIn');
goog.require('JaSMIn.DynamicRobotModel');
goog.require('JaSMIn.SoccerBot2DSpecification');





/**
 * SoccerBot2D Constructor
 *
 * @constructor
 * @extends {JaSMIn.DynamicRobotModel}
 * @param {!string} name the name of the agent model
 * @param {!JaSMIn.SoccerBot2DSpecification} specification the soccer bot 2d specification
 * @param {!JaSMIn.ParameterMap} environmentParams the environment paraméter
 * @param {!JaSMIn.MeshFactory} meshFactory the mesh factory
 */
JaSMIn.SoccerBot2D = function(name, specification, meshFactory, environmentParams) {
  goog.base(this, name, specification, meshFactory);

  /**
   * The list of stamina materials.
   * @type {!Array<!THREE.Material>}
   */
  this.staminaMatList = [];

  /**
   * The maximum stamina value.
   * @type {!number}
   */
  this.maxStamina = 8000;
  var maxStaminaParam = environmentParams.getNumber(JaSMIn.D2.EnvironmentParams.STAMINA_MAX);
  if (maxStaminaParam !== null) {
    this.maxStamina = maxStaminaParam;
  }

  // Extract stamina materials
  var i = specification.staminaMaterialNames.length;
  while (i--) {
    var mat = meshFactory.materialCache[specification.staminaMaterialNames[i]];
    if (mat !== undefined) {
      this.staminaMatList.push(mat);
    }
  }
};
goog.inherits(JaSMIn.SoccerBot2D, JaSMIn.DynamicRobotModel);



/**
 * Update the joint objects according to the given angles.
 *
 * @param  {!Array<number>} data the agent data of the current state
 * @param  {Array<number>=} nextData the agent data of the next state
 * @param  {!number=} t the interpolation time
 * @return {void}
 */
JaSMIn.SoccerBot2D.prototype.updateData = function(data, nextData, t) {
  if (data[JaSMIn.D2.AgentData.STAMINA] === undefined) {
    return;
  }

  var stamina = nextData === undefined ?
        data[JaSMIn.D2.AgentData.STAMINA] :
        data[JaSMIn.D2.AgentData.STAMINA] + (nextData[JaSMIn.D2.AgentData.STAMINA] - data[JaSMIn.D2.AgentData.STAMINA]) * t;
  stamina = THREE.Math.clamp(stamina, 0, this.maxStamina);
  stamina = (this.maxStamina - stamina) / this.maxStamina;

  // Apply stamina color
  var i = this.staminaMatList.length;
  while (i--) {
    var mat = this.staminaMatList[i];
    if (mat.color.r === stamina) {
      // Prevent material updates if stamina value hasn't changed (e.g. on pausing)
      break;
    }
    mat.color.setScalar(stamina);
    mat.needsUpdate = true;
  }
};
