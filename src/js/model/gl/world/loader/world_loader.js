/**
 * The WorldLoader class definition.
 *
 * The WorldLoader provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.WorldLoader');

goog.require('JaSMIn');
goog.require('JaSMIn.Agent');
goog.require('JaSMIn.Ball');
goog.require('JaSMIn.Field');
// goog.require('JaSMIn.GameLog');
goog.require('JaSMIn.NaoModelFactory');
goog.require('JaSMIn.ParameterMap');
goog.require('JaSMIn.RobotModelFactory');
// goog.require('JaSMIn.SceneUtils');
goog.require('JaSMIn.SoccerBot2DModelFactory');
goog.require('JaSMIn.Team');
goog.require('JaSMIn.TeamDescription');
// goog.require('JaSMIn.World');
goog.require('JaSMIn.WorldModelFactory');
// goog.require('JaSMIn.WorldParameter');





/**
 * WorldLoader Constructor
 *
 * @constructor
 */
JaSMIn.WorldLoader = function() {

  /**
   * The world model factory for 2D and 3D simulation environments.
   * @type {!JaSMIn.WorldModelFactory}
   */
  this.worldModelFactory = new JaSMIn.WorldModelFactory();

  /**
   * The list of robot model factories for 2D simulation models.
   * @type {!Array<!JaSMIn.RobotModelFactory>}
   */
  this.modelFactories2D = [new JaSMIn.SoccerBot2DModelFactory()];

  /**
   * The list of robot model factories for 3D simulation models.
   * @type {!Array<!JaSMIn.RobotModelFactory>}
   */
  this.modelFactories3D = [new JaSMIn.NaoModelFactory()];
};



/**
 * Dispose all resources allocated within this instance.
 *
 * @return {void}
 */
JaSMIn.WorldLoader.prototype.dispose = function() {

};



/**
 * Create a new default world scene representation, containing a sky-box and standard lighting.
 *
 * @return {!THREE.Scene} the new world scene object
 */
JaSMIn.WorldLoader.prototype.create = function() {
  var scene = new THREE.Scene();
  scene.name = 'soccerScene';

  this.worldModelFactory.createScene(scene);

  return scene;
};



/**
 * Create a ball representation.
 *
 * @param  {!JaSMIn.Ball} ball the ball representation
 * @return {void}
 */
JaSMIn.WorldLoader.prototype.createBall = function(ball) {
  this.worldModelFactory.createBall(ball);
};



/**
 * Update the scene representation.
 *
 * @param  {!THREE.Scene} scene the scene instance
 * @param  {!THREE.Vector2} fieldDimensions the field dimensions
 * @return {void}
 */
JaSMIn.WorldLoader.prototype.updateScene = function(scene, fieldDimensions) {
  this.worldModelFactory.updateScene(scene, fieldDimensions);
}



/**
 * Update the field representation for the given field instance.
 * This function places and rescales the field and border objects according to the field dimensions.
 * Furthermore, a new set of field lines is created.
 *
 * @param  {!JaSMIn.Field} field the field instance
 * @return {void}
 */
JaSMIn.WorldLoader.prototype.updateField = function(field) {
  this.worldModelFactory.updateField(field);
}



/**
 * Load a team representation.
 * This method can be called repeatedly to load additional agents to a team,
 * as well as additional robot models to all agents of the team (this comes handy for streaming).
 *
 * @param  {!JaSMIn.GameType} type the world type (2D or 3D)
 * @param  {!JaSMIn.Team} team the team representation
 * @param  {!JaSMIn.ParameterMap} environmentParams the environment parameter
 * @param  {!JaSMIn.ParameterMap} playerParams the player paraméter
 * @return {!JaSMIn.Team} the given team
 */
JaSMIn.WorldLoader.prototype.loadTeam = function(type, team, environmentParams, playerParams) {
  var teamDescription = team.description;

  for (var i = 0; i < teamDescription.agents.length; ++i) {
    var agent = team.agents[i];

    // Create agent representation if not yet present
    if (agent === undefined) {
      agent = new JaSMIn.Agent(teamDescription.agents[i]);
      team.agents[i] = agent;
      team.objGroup.add(agent.objGroup);
    }

    // Create robot models for agent if not yet present
    for (var j = 0; j < agent.description.playerTypes.length; ++j) {
      if (agent.models[j] === undefined) {
        var model = this.createModel(type,
                                     agent.description.playerTypes[j],
                                     teamDescription.side,
                                     agent.description.playerNo,
                                     environmentParams,
                                     playerParams);

        if (model !== null) {
          agent.models[j] = model;
          agent.models[j].setTeamColor(team.color);
          agent.objGroup.add(agent.models[j].objGroup);
        }
      }
    }
  }

  return team;
};



/**
 * Load a team representation.
 * This method can be called repeatedly to load additional agents to a team,
 * as well as additional robot models to all agents of the team (this comes handy for streaming).
 *
 * @param  {!JaSMIn.GameType} gameType the world type (2D or 3D)
 * @param  {!JaSMIn.ParameterMap} playerType the player type
 * @param  {!JaSMIn.TeamSide} side the team side
 * @param  {!number} playerNo the player number
 * @param  {!JaSMIn.ParameterMap} environmentParams the environment paraméter
 * @param  {!JaSMIn.ParameterMap} playerParams the player parameter
 * @return {?JaSMIn.RobotModel} the new robot model, or null in case of an unknown model
 */
JaSMIn.WorldLoader.prototype.createModel = function(gameType, playerType, side, playerNo, environmentParams, playerParams) {
  var modelFactories = gameType === JaSMIn.GameType.TWOD ? this.modelFactories2D : this.modelFactories3D;
  var i = modelFactories.length;
  var model = null;

  while (i--) {
    model = modelFactories[i].createModel(playerType, side, playerNo, environmentParams, playerParams);

    if (model !== null) {
      break;
    }
  }

  return model;
};
