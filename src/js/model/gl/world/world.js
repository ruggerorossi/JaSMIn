/**
 * The World class definition.
 *
 * The World provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.World');

goog.require('JaSMIn.Ball');
goog.require('JaSMIn.Field');
goog.require('JaSMIn.ParameterMap');
goog.require('JaSMIn.Team');
goog.require('JaSMIn.WorldLoader');
goog.require('JaSMIn.WorldState');





/**
 * the world events enum.
 * @enum {!string}
 */
JaSMIn.WorldEvents = {
  CHANGE: 'change'
};



/**
 * World Constructor
 *
 * @constructor
 * @implements {JaSMIn.IPublisher}
 * @implements {JaSMIn.IEventDispatcher}
 */
JaSMIn.World = function() {

  /**
   * The world loader instance.
   * @type {!JaSMIn.WorldLoader}
   */
  this.worldLoader = new JaSMIn.WorldLoader();

  /**
   * The game type this world is representing.
   * @type {!JaSMIn.GameType}
   */
  this.type = JaSMIn.GameType.TWOD;

  /**
   * The list of server/simulation environment parameters.
   * @type {!JaSMIn.ParameterMap}
   */
  this.environmentParams = new JaSMIn.ParameterMap();

  /**
   * The list of player parameters.
   * @type {!JaSMIn.ParameterMap}
   */
  this.playerParams = new JaSMIn.ParameterMap();

  /**
   * The list of player types.
   * @type {!Array<!JaSMIn.ParameterMap>}
   */
  this.playerTypes = [];

  /**
   * The world scene.
   * @type {!THREE.Scene}
   */
  this.scene = this.worldLoader.create();

  /**
   * The soccer field object.
   * @type {!JaSMIn.Field}
   */
  this.field = new JaSMIn.Field();
  this.scene.add(this.field.objGroup);

  // Create field representation
  this.worldLoader.updateField(this.field);

  /**
   * The soccer ball object.
   * @type {!JaSMIn.Ball}
   */
  this.ball = new JaSMIn.Ball();
  this.scene.add(this.ball.objGroup);
  this.scene.add(this.ball.objTwoDGroup);

  // Create ball representation
  this.worldLoader.createBall(this.ball);

  /**
   * The left team.
   * @type {!JaSMIn.Team}
   */
  this.leftTeam = new JaSMIn.Team(new JaSMIn.TeamDescription('left', new THREE.Color('#0000ff'), JaSMIn.TeamSide.LEFT));
  this.scene.add(this.leftTeam.objGroup);

  /**
   * The right team.
   * @type {!JaSMIn.Team}
   */
  this.rightTeam = new JaSMIn.Team(new JaSMIn.TeamDescription('right', new THREE.Color('#ff0000'), JaSMIn.TeamSide.RIGHT));
  this.scene.add(this.rightTeam.objGroup);

  /**
   * The world bounds.
   * @type {!THREE.Vector3}
   */
  this.boundingBox = new THREE.Vector3(512, 512, 512);
};



/**
 * Create a (new) world representation for the given game log and update this world instance accordingsly.
 *
 * @param  {!JaSMIn.GameType} type the world (replay) type (2D or 3D)
 * @param  {!JaSMIn.ParameterMap} environmentParams the environment paraméter
 * @param  {!JaSMIn.ParameterMap} playerParams the player parameter
 * @param  {!Array<!JaSMIn.ParameterMap>} playerTypes the player types list
 * @param  {!JaSMIn.TeamDescription} leftTeamDescription the left team description
 * @param  {!JaSMIn.TeamDescription} rightTeamDescription the right team description
 * @return {void}
 */
JaSMIn.World.prototype.create = function(type, environmentParams, playerParams, playerTypes, leftTeamDescription, rightTeamDescription) {
  // Update parameters
  this.type = type;
  this.environmentParams = environmentParams;
  this.playerParams = playerParams;
  this.playerTypes = playerTypes;


  // Update field representation
  this.field.set(type, this.environmentParams);
  this.worldLoader.updateField(this.field);


  // Resize ball
  // var ballRadius = /** @type {!number} */ (environmentParams[JaSMIn.EnvironmentParams2D.BALL_SIZE]);
  // if (!ballRadius) {
  //   ballRadius = type === JaSMIn.GameType.TWOD ? 0.2 : 0.042;
  // }
  this.ball.setRadius(type === JaSMIn.GameType.TWOD ? 0.2 : 0.042);


  // Reset and load teams
  this.leftTeam.set(leftTeamDescription);
  this.rightTeam.set(rightTeamDescription);
  this.updateTeams(type);


  // Update light shadow cone to closely match field
  this.worldLoader.updateScene(this.scene, this.field.fieldDimensions);


  // Publish change event
  this.dispatchEvent({
    type: JaSMIn.WorldEvents.CHANGE
  });
};



/**
 * Update representations of teams.
 *
 * @param  {!JaSMIn.GameType} type the world (replay) type (2D or 3D)
 * @return {void}
 */
JaSMIn.World.prototype.updateTeams = function(type) {
  // Reload teams
  this.worldLoader.loadTeam(type, this.leftTeam, this.environmentParams, this.playerParams);
  this.worldLoader.loadTeam(type, this.rightTeam, this.environmentParams, this.playerParams);
};



/**
 * Enable or disable shaddows.
 *
 * @param {!boolean} enabled true to enable shadows, false to disable
 */
JaSMIn.World.prototype.setShadowsEnabled = function(enabled) {
  var i = this.scene.children.length;

  while (i--) {
    var child = this.scene.children[i];

    if (child.type == 'DirectionalLight' ||
        child.type == 'PointLight' ||
        child.type == 'SpotLight') {
      child.castShadow = enabled;
    }
  }
};



/**
 * Update world objects.
 *
 * @param  {!JaSMIn.WorldState} state the current world state
 * @param  {!JaSMIn.WorldState} nextState the next world state
 * @param  {!number} t the interpolation time
 * @return {void}
 */
JaSMIn.World.prototype.update = function(state, nextState, t) {
  this.ball.update(state.ballState, nextState.ballState, t);

  this.leftTeam.update(state.leftAgentStates, nextState.leftAgentStates, t);
  this.rightTeam.update(state.rightAgentStates, nextState.rightAgentStates, t);
};



/**
 * Dispose this world.
 *
 * @return {void}
 */
JaSMIn.World.prototype.dispose = function() {
  // TODO: Dispose threejs scene and other objects
  this.worldLoader.dispose();
};



// ============================== EVENT DISPATCHER FUNCTIONS ==============================
/** @override */
JaSMIn.World.prototype.addEventListener = JaSMIn.EventDispatcher.prototype.addEventListener;
/** @override */
JaSMIn.World.prototype.removeEventListener = JaSMIn.EventDispatcher.prototype.removeEventListener;
/** @override */
JaSMIn.World.prototype.dispatchEvent = JaSMIn.EventDispatcher.prototype.dispatchEvent;
// ============================== EVENT DISPATCHER FUNCTIONS ==============================
