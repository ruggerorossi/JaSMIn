/**
 * The GameState class definition.
 *
 * The GameState provides information about the current state of a game.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.GameState');



/**
 * GameState Constructor
 * Create a new GameState holding the game state information.
 *
 * @constructor
 * @struct
 * @param {!number} time
 * @param {!string} playMode
 */
JaSMIn.GameState = function(time, playMode) {

  /**
   * The global time when this state was reached.
   * @type {!number}
   */
  this.time = time;

  /**
   * The play mode string.
   * @type {!string}
   */
  this.playMode = playMode;
};



/**
 * Fetch the play mode string.
 *
 * @return {!string} the play mode string
 */
JaSMIn.GameState.prototype.getPlayModeString = function() {
  return this.playMode;
};
