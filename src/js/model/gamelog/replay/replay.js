/**
 * The Replay class definition.
 *
 * The Replay is the central class holding a replay file
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.Replay');

goog.require('JaSMIn');
goog.require('JaSMIn.PartialWorldState');





/**
 * Replay Constructor
 * Create a new replay.
 *
 * @constructor
 * @extends {JaSMIn.GameLog}
 * @param {!JaSMIn.GameType} type the game-log type
 * @param {!number} version the replay version
 */
JaSMIn.Replay = function(type, version) {
  goog.base(this, type);

  /**
   * The replay version.
   * @type {!number}
   */
  this.version = version;
};
goog.inherits(JaSMIn.Replay, JaSMIn.GameLog);
