/**
 * The GameLog class definition.
 *
 * The GameLog is the central class holding a game log.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.GameLog');

goog.require('JaSMIn');
goog.require('JaSMIn.GameScore');
goog.require('JaSMIn.GameState');
goog.require('JaSMIn.ParameterMap');
goog.require('JaSMIn.TeamDescription');
goog.require('JaSMIn.WorldState');



/**
 * @enum {!string}
 */
JaSMIn.GameLogChangeEvents = {
  TEAMS: 'teams',
  STATES: 'states'
};






/**
 * GameLog Constructor
 * Create a new game log.
 *
 * @constructor
 * @param {!JaSMIn.GameType} type the game type
 */
JaSMIn.GameLog = function(type) {

  /**
   * The log file url.
   * @type {?string}
   */
  this.url = null;

  /**
   * The game type (2D or 3D).
   * @type {!JaSMIn.GameType}
   */
  this.type = type;

  /**
   * The state update frequency of the game log.
   * @type {!number}
   */
  this.frequency = 1;

  /**
   * The list of server/simulation environment parameters.
   * @type {!JaSMIn.ParameterMap}
   */
  this.environmentParams = new JaSMIn.ParameterMap();

  /**
   * The list of player parameters.
   * @type {!JaSMIn.ParameterMap}
   */
  this.playerParams = new JaSMIn.ParameterMap();

  /**
   * The list of player type parameters.
   * @type {!Array<!JaSMIn.ParameterMap>}
   */
  this.playerTypes = [];

  /**
   * The description of the left team.
   * @type {!JaSMIn.TeamDescription}
   */
  this.leftTeam = new JaSMIn.TeamDescription('Left Team', new THREE.Color(0xffff00), JaSMIn.TeamSide.LEFT);

  /**
   * The description of the right team.
   * @type {!JaSMIn.TeamDescription}
   */
  this.rightTeam = new JaSMIn.TeamDescription('Right Team', new THREE.Color(0xff0000), JaSMIn.TeamSide.RIGHT);

  /**
   * The list of all world states.
   * @type {!Array<!JaSMIn.WorldState>}
   */
  this.states = [];

  /**
   * The time value of the first state.
   * @type {!number}
   */
  this.startTime = 0;

  /**
   * The time value of the last state.
   * @type {!number}
   */
  this.endTime = 0;

  /**
   * The duration of the game log.
   * @type {!number}
   */
  this.duration = 0;

  /**
   * A list of game states over time.
   * @type {!Array<!JaSMIn.GameState>}
   */
  this.gameStateList = [];

  /**
   * A list of game scores over time.
   * @type {!Array<!JaSMIn.GameScore>}
   */
  this.gameScoreList = [];

  /**
   * Indicator if the game log is fully loaded.
   * @type {!boolean}
   */
  this.fullyLoaded = false;

  /**
   * The callback function to call when this game log instance is refreshed
   * @type {!Function | undefined}
   */
  this.onChange = undefined;

  // Create defaults
  if (type === JaSMIn.GameType.TWOD) {
    this.environmentParams = JaSMIn.D2.createDefaultEnvironmentParams();
    this.playerParams = JaSMIn.D2.createDefaultPlayerParams();
    this.playerTypes = JaSMIn.D2.createDefaultPlayerTypeParams();
  } else {
    this.environmentParams = JaSMIn.D3.createDefaultEnvironmentParams();
    this.playerParams = JaSMIn.D3.createDefaultPlayerParams();
    this.playerTypes = JaSMIn.D3.createDefaultPlayerTypeParams();
  }

  this.updateFrequency();
};



/**
 * Update the frequency value from environment parameter list.
 *
 * @return {void}
 */
JaSMIn.GameLog.prototype.updateFrequency = function() {
  var step = null;

  if (this.type === JaSMIn.GameType.TWOD) {
    step = this.environmentParams.getNumber(JaSMIn.D2.EnvironmentParams.SIMULATOR_STEP);
  } else {
    step = this.environmentParams.getNumber(JaSMIn.D3.EnvironmentParams.LOG_STEP);
  }

  if (step) {
    this.frequency = 1000 / step;
  }
};



/**
 * Fetch the index of the world state that corresponds to the given time.
 *
 * @param  {!number} time the global time
 * @return {!number} the world state index corresponding to the specified time
 */
JaSMIn.GameLog.prototype.getIndexForTime = function(time) {
  var idx = Math.floor(time * this.frequency);

  if (idx < 0) {
    return 0;
  } else if (idx >= this.states.length) {
    return this.states.length - 1;
  }

  return idx;
};



/**
 * Retrieve the world state for the given time.
 *
 * @param  {!number} time the global time
 * @return {JaSMIn.WorldState} the world state closest to the specified time
 */
JaSMIn.GameLog.prototype.getStateForTime = function(time) {
  return this.states[this.getIndexForTime(time)];
};



/**
 * Called to indicate that the team descriptions of the game log were updated.
 *
 * @return {void}
 */
JaSMIn.GameLog.prototype.onTeamsUpdated = function() {
  if (this.onChange !== undefined) {
    this.onChange(JaSMIn.GameLogChangeEvents.TEAMS);
  }
};



/**
 * Called to indicate that the game log data was changed/extended.
 *
 * @return {void}
 */
JaSMIn.GameLog.prototype.onStatesUpdated = function() {
  // Update times
  if (this.states.length > 0) {
    this.startTime = this.states[0].time;
    this.endTime = this.states[this.states.length - 1].time;
    this.duration = this.endTime - this.startTime;

    // Extract game states and scores from state array
    this.gameStateList = [];
    this.gameScoreList = [];

    var previousGameState = this.states[0].gameState;
    var previousScore = this.states[0].score;
    this.gameStateList.push(previousGameState);
    this.gameScoreList.push(previousScore);

    for (var i = 1; i < this.states.length; i++) {
      if (previousGameState !== this.states[i].gameState) {
        previousGameState = this.states[i].gameState;
        this.gameStateList.push(previousGameState);
      }

      if (previousScore !== this.states[i].score) {
        previousScore = this.states[i].score;
        this.gameScoreList.push(previousScore);
      }
    }
  }

  if (this.onChange !== undefined) {
    this.onChange(JaSMIn.GameLogChangeEvents.STATES);
  }
};



/**
 * Called to indicate that the game log file is fully loaded and parsed and no further states, etc. will be appended.
 *
 * @return {void}
 */
JaSMIn.GameLog.prototype.finalize = function() {
  this.fullyLoaded = true;

  // Refresh the game log information a last time and publish change to finished
  this.onStatesUpdated();

  // Clear onChange listener
  this.onChange = undefined;
};
