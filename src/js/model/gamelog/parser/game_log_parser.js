/**
 * The GameLogParser interface definition.
 *
 * The GameLogParser...
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.GameLogParser');

goog.require('JaSMIn.GameLog');



/**
 * GameLogParser Interface.
 * @interface
 */
JaSMIn.GameLogParser = function() {};



/**
 * Parse the given data into a game log data structure.
 *
 * @param {!string} data the game log file data
 * @param {!JaSMIn.DataExtent=} extent the data extent: complete, partial or incremental data (default: complete)
 * @return {!boolean} true, if a new game log file instance was created, false otherwise
 */
JaSMIn.GameLogParser.prototype.parse = function(data, extent) {};



/**
 * Retrieve the currently parsed game log.
 *
 * @return {?JaSMIn.GameLog} the (maybe partially) parsed game log
 */
JaSMIn.GameLogParser.prototype.getGameLog = function() {};



/**
 * Dispose all resources referenced in this parser instance.
 *
 * @param {!boolean=} keepIteratorAlive indicator if iterator should not be disposed
 * @return {void}
 */
JaSMIn.GameLogParser.prototype.dispose = function(keepIteratorAlive) {};