/**
 * The PartialWorldState class definition.
 *
 * The PartialWorldState provides information about the state of the game, the ball and all agents on the field.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.PartialWorldState');

goog.require('JaSMIn.GameScore');
goog.require('JaSMIn.GameState');
goog.require('JaSMIn.AgentState');
goog.require('JaSMIn.ObjectState');
goog.require('JaSMIn.WorldState');



/**
 * PartialWorldState Constructor
 * Create a new PartialWorldState holding the given information.
 *
 * @constructor
 * @struct
 * @param {!number} time the global time
 * @param {!number} timeStep the global time step
 * @param {!number} gameTime the game time
 */
JaSMIn.PartialWorldState = function(time, timeStep, gameTime) {
  /**
   * The global time.
   * @type {!number}
   */
  this.time = time;

  /**
   * The global time step.
   * @type {!number}
   */
  this.timeStep = timeStep;

  /**
   * The game time.
   * @type {!number}
   */
  this.gameTime = gameTime;

  /**
   * The state of the game.
   * @type {!JaSMIn.GameState}
   */
  this.gameState = new JaSMIn.GameState(time, 'unknown');

  /**
   * The left team score.
   * @type {!JaSMIn.GameScore}
   */
  this.score = new JaSMIn.GameScore(time, 0, 0);

  /**
   * The state of the ball.
   * @type {!JaSMIn.ObjectState}
   */
  this.ballState = new JaSMIn.ObjectState(new THREE.Vector3(0, 0, 0), new THREE.Quaternion());

  /**
   * The states of all left agents.
   * @type {!Array<!JaSMIn.AgentState | undefined>}
   */
  this.leftAgentStates = [];

  /**
   * The states of all right agents.
   * @type {!Array<!JaSMIn.AgentState | undefined>}
   */
  this.rightAgentStates = [];
};



/**
 * Reinitialize the gameTime attribute.
 *
 * @param {!number} gameTime the game time
 */
JaSMIn.PartialWorldState.prototype.setGameTime = function(gameTime) {
  this.gameTime = Math.round(gameTime * 1000) / 1000;
};



/**
 * Reinitialize the gameState and gameTime attributes.
 *
 * @param {!string} playMode the play mode string (will create a copy if needed)
 */
JaSMIn.PartialWorldState.prototype.setPlaymode = function(playMode) {
  if (this.gameState.playMode !== playMode) {
    this.gameState = new JaSMIn.GameState(this.time, JaSMIn.copyString(playMode));
  }
};



/**
 * Reinitialize the gameScore and gameTime attributes.
 *
 * @param {!number} goalsLeft the left team score
 * @param {!number} goalsRight the right team score
 * @param {!number=} penScoreLeft the left team penalty score
 * @param {!number=} penMissLeft the left team penalty misses
 * @param {!number=} penScoreRight the right team penalty score
 * @param {!number=} penMissRight the right team penalty misses
 */
JaSMIn.PartialWorldState.prototype.setScore = function(goalsLeft, goalsRight, penScoreLeft, penMissLeft, penScoreRight, penMissRight) {
  if (penScoreLeft === undefined) {
    penScoreLeft = 0;
  }
  if (penMissLeft === undefined) {
    penMissLeft = 0;
  }
  if (penScoreRight === undefined) {
    penScoreRight = 0;
  }
  if (penMissRight === undefined) {
    penMissRight = 0;
  }

  if (this.score.goalsLeft !== goalsLeft ||
      this.score.goalsRight !== goalsRight ||
      this.score.penaltyScoreLeft !== penScoreLeft ||
      this.score.penaltyMissLeft !== penMissLeft ||
      this.score.penaltyScoreRight !== penScoreRight ||
      this.score.penaltyMissRight !== penMissRight) {
    this.score = new JaSMIn.GameScore(this.time, goalsLeft, goalsRight, penScoreLeft, penMissLeft, penScoreRight, penMissRight);
  }
};



/**
 * Create a new world state instance from this partial world state and append it to the list.
 * A new world state is only created if more then one agent state is present.
 *
 * @param  {!Array<!JaSMIn.WorldState>} states the world state list
 * @return {!boolean} true, if a new world state was appended, false otherwise
 */
JaSMIn.PartialWorldState.prototype.appendTo = function(states) {
  if (this.leftAgentStates.length + this.rightAgentStates.length > 0) {
    states.push(new JaSMIn.WorldState(
        this.time,
        this.gameTime,
        this.gameState,
        this.score,
        this.ballState,
        this.leftAgentStates,
        this.rightAgentStates)
      );

    // Progress time
    this.time = Math.round((this.time + this.timeStep) * 1000) / 1000;

    // Reset agent states
    this.leftAgentStates = [];
    this.rightAgentStates = [];

    return true;
  }

  return false;
};
