/**
 * The ParameterMap class definition.
 *
 * The ParameterMap provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.ParameterMap');



/**
 * ParameterMap Constructor
 *
 * @constructor
 * @param {!Object=} params the parameter object.
 */
JaSMIn.ParameterMap = function(params) {

  /**
   * The parameter object.
   * @type {!Object}
   */
  this.paramObj = params !== undefined ? params : {};
};



/**
 * Clear this parameter map.
 *
 * @return {void}
 */
JaSMIn.ParameterMap.prototype.clear = function() {
  this.paramObj = {};
};



/**
 * Fetch a number parameter with the given key.
 * This method will return null if:
 * - the key is invalid (undefined)
 * - the value with the given key is not a number
 *
 * @param {!string | !number} key the key of interest
 * @return {?number}
 */
JaSMIn.ParameterMap.prototype.getNumber = function(key) {
  var value = this.paramObj[key];

  if (value !== undefined && typeof value === 'number') {
    return value;
  }

  return null;
};



/**
 * Fetch a boolean parameter with the given key.
 * This method will return null if:
 * - the key is invalid (undefined)
 *
 * @param {!string | !number} key the key of interest
 * @return {?boolean}
 */
JaSMIn.ParameterMap.prototype.getBoolean = function(key) {
  var value = this.paramObj[key];

  if (value !== undefined) {
    return value ? true : false;
  }

  return null;
};



/**
 * Fetch a string parameter with the given key.
 * This method will return null if:
 * - the key is invalid (undefined)
 * - the value with the given key is not a string
 *
 * @param {!string | !number} key the key of interest
 * @return {?string}
 */
JaSMIn.ParameterMap.prototype.getString = function(key) {
  var value = this.paramObj[key];

  if (value !== undefined && typeof value === 'string') {
    return value;
  }

  return null;
};



/**
 * Fetch a new parameter wrapper object for the object with the given key.
 * This method will return null if:
 * - the key is invalid (undefined)
 * - the value with the given key is not an object
 *
 * @param {!string | !number} key the key of interest
 * @return {?JaSMIn.ParameterMap}
 */
JaSMIn.ParameterMap.prototype.getObject = function(key) {
  var value = this.paramObj[key];

  if (value !== undefined && value !== null && typeof value === 'object') {
    return new JaSMIn.ParameterMap(value);
  }

  return null;
};
