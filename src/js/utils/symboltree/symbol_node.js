/**
 * The SymbolNode class definition.
 *
 * The SymbolNode represents a symbolic node in a symbol tree, holding values and child nodes.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.SymbolNode');

goog.require('JaSMIn');



/**
 * SymbolNode Constructor
 * Create a new SymbolNode.
 *
 * @constructor
 * @struct
 */
JaSMIn.SymbolNode = function() {

  /**
   * The symbol node values.
   * @type {!Array<!string>}
   */
  this.values = [];

  /**
   * The symbol node children.
   * @type {!Array<!JaSMIn.SymbolNode>}
   */
  this.children = [];
};
